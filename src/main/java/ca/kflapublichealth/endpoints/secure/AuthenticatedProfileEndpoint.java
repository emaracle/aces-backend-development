package ca.kflapublichealth.endpoints.secure;
import java.util.HashMap;
import java.util.LinkedHashMap;

import java.io.IOException;
import java.net.URISyntaxException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ca.kflapublichealth.requests.RequestManager;
import net.oauth.OAuthException;

@Path("/secure/profile/")
public class AuthenticatedProfileEndpoint {

    @POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("{UserUUID}/updateUserProfile")
	public static String updateUserProfile(
            @QueryParam("token") String token,
			@PathParam("UserUUID") String UserUUID,
			@FormParam("FName") String FName,
			@FormParam("LName") String LName,
			@FormParam("Email") String Email,
			@FormParam("Telephone") String Telephone,
			@FormParam("Position") String Position,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) throws OAuthException, IOException, URISyntaxException {

        java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}

		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("token", token);
		params.put("UserUUID", UserUUID);
		params.put("FName", FName);
		params.put("LName", LName);
		params.put("Email", Email);
		params.put("Position", Position);
		params.put("Telephone", Telephone);

        HashMap<String, String> response = RequestManager.getAuthenticatedResponse("json", params, request, 0, "updateUserProfile");
        if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}
		return response.get("Data");
	}
}