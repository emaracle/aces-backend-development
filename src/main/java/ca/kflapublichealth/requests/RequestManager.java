package ca.kflapublichealth.requests;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import ca.kflapublichealth.connections.DatabaseConnection;
import ca.kflapublichealth.output.OutputFormat;

/**
 * Manages the requests made by breaking down the request and executing all
 * components seperately before returning the requests in the appropriate format
 *
 * @author wzaghal
 *
 */
public class RequestManager {

	static final Logger logger = LogManager.getLogger(RequestManager.class);
	private static String format;
	static public RequestReference reqReference = new RequestReference();

	/**
	 * Returns the remote ip of the request passed.
	 * Looks for X-FORWARDED-FOR header and if found, returns it's value and getRemoteAddr() value if not.
	 *
	 * @param request Request object
	 */
	public static String getRemoteAddr(HttpServletRequest request) {
		String remoteAddr = request.getHeader("X-FORWARDED-FOR");
		if (remoteAddr == null || remoteAddr.length() == 0) {
			remoteAddr = request.getRemoteAddr();
		}
		remoteAddr = remoteAddr.split(",")[0];

		return remoteAddr;
	}

	public static HashMap<String, String> getResponse(String format,
			ArrayList<Object> sqlParams, LinkedHashMap<String,Object> linkedSqlParams, HttpServletRequest request, int page,
			String requestID) {

		// Gathering Data about request and cross-checking with RequestReference
		Request req = reqReference.getRequest(requestID);
		if (req==null){
			logger.warn("Req is null");
		}

		String storedProcedure = req.getStoredProcedure();
		RequestManager.format = format.toLowerCase();
		HashMap<String, String> response = new HashMap<String, String>();

		String user = null;
		if (requestID=="getUserIDDB") {
			user = (String)linkedSqlParams.get("userid");
		}

		String token = null;
		// get token
		if (linkedSqlParams!=null) {
			token = (String)linkedSqlParams.get("token");
			if (removeTokenParm(requestID)) { // don't remove params for some SPROCs
				linkedSqlParams.remove("token");
			}
		}

		if (format.equals("xml") || format.equals("json") || format.equals("detect")) {
			ResultSet dataResults = null;

			if (requireToken(requestID)==false || (token!=null && RequestAuthenticator.validToken(token))) {	// require token check

				// Get Cached result set
				if (sqlParams==null && linkedSqlParams==null) {
					dataResults = DatabaseConnection.getData(storedProcedure);
				} else if (sqlParams!=null) {
					dataResults = DatabaseConnection.getData(storedProcedure, sqlParams);
				} else if (linkedSqlParams!=null) {
					dataResults = DatabaseConnection.getData(storedProcedure, linkedSqlParams);
				}

				ArrayList<Object> resultList = RequestProcessor.getDataFromResultSet(dataResults); // get data from result

				//If format is unknown at time of request
				if (format.equals("detect")){

					if (((HashMap)resultList.get(0)).get("Settings") != null){
						//Get the settings
						String strSettings = ((HashMap)resultList.get(0)).get("Settings").toString();

						//Check if valid JSON
						Boolean isJsonValid = false;
						isJsonValid = OutputFormat.isJSONValid(strSettings);

						//If not JSON (xml), convert to JSON
						if (!isJsonValid){
							strSettings = OutputFormat.xmlToJson(strSettings);
							((HashMap)resultList.get(0)).put("Settings",strSettings);
						}
					}

					//Set process format to Json
					format = "json";
				}

				if (resultList.isEmpty() && (requestID.equals("getUserIDDB") || requestID.equals("changePassword"))) {	// login failed
					auditLog(request, javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED, token, user, null);
					throw new WebApplicationException(javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED);

				} else {
					if (format.equals("xml")) {
						if (requestID.equals("getAlertPacket")) {
							response.put("Data", "<xml>" + ((HashMap)resultList.get(0)).get("XML").toString() + "</xml>");
						} else {
							response.put("Data", OutputFormat.toXml(resultList));
						}

					} else if (format.equals("json")) {
						if (requestID.equals("getVisitDetails")) {
							response.put("Data", OutputFormat.toJsonVisitDetail(resultList));
						} else {
							response.put("Data", OutputFormat.toJson(resultList));
						}
					}


					if (requestID.equals("getUserIDDB")){
						HashMap<String, Object> row = (HashMap<String, Object>) resultList.get(0);

						if (row.containsKey("Code") == true) {
							int code = (int) row.get("Code");
							switch (code) {
								//1 - IP Blocked
								case 1:
									auditLog(request, 423, token, user, response.get("Data"));
									throw new WebApplicationException(423);
								//2 - User Blocked
								case 2:
									auditLog(request, 423, token, user, response.get("Data"));
									throw new WebApplicationException(423);
								//400 - Username doesn't exist	
								case 400:
									auditLog(request, 400, token, user, response.get("Data"));
									throw new WebApplicationException(400);
								//429 - Next failed login will block user
								case 429:
								  auditLog(request, 429, token, user, response.get("Data"));
								  auditLog(request, 423, token, user, response.get("Data"));
								  throw new WebApplicationException(429);
							}
						}
					}

					// SQL errors when attempting to lookup user for forgot username/password and e-mail them
					if ((requestID.equals("forgotUsername") || requestID.equals("forgotPassword")) && resultList.get(0).toString().startsWith("{Status=ERROR:")) {
						auditLog(request, javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR, token, user, response.get("Data"));
						if(!(response.get("Data")).equalsIgnoreCase("[{\"Status\":\"ERROR: No such username.\"}]") &&
						   !(response.get("Data")).equalsIgnoreCase("[{\"Status\":\"ERROR: Account is locked.\"}]"))
						{
							throw new WebApplicationException(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
						}
					}

					// Throw exception if new password is the same as old password
					if (requestID.equals("checkPassword") && resultList.get(0).toString().equals("{PasswordMatch=true}")) {
						auditLog(request, javax.servlet.http.HttpServletResponse.SC_FORBIDDEN, token, user, response.get("Data"));
						throw new WebApplicationException(javax.servlet.http.HttpServletResponse.SC_FORBIDDEN);
					}

					// Check if the returned resultset has column named code. Throw error based on its value.
					if (requestID.equals("getCustomUserKeywordAlerts") || requestID.equals("addCustomUserKeywordAlerts") ||
					    requestID.equals("updateCustomUserKeywordAlerts") || requestID.equals("deleteCustomUserKeywordAlerts") || requestID.equals("validateCustomUserKeywordAlerts")) {
						if (resultList.size() > 0) {
							@SuppressWarnings("unchecked")
							HashMap<Object, Object> row = ((HashMap<Object, Object>) resultList.get(0));
							if (row.containsKey("Code")) {
								switch(Integer.parseInt(row.get("Code").toString())) {
								case 1:
									auditLog(request, javax.servlet.http.HttpServletResponse.SC_FORBIDDEN, token, user, response.get("Data"));
									throw new WebApplicationException(javax.servlet.http.HttpServletResponse.SC_FORBIDDEN);
								case 2:
									auditLog(request, javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST, token, user, response.get("Data"));
									throw new WebApplicationException(row.get("Status").toString(), javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST);
								}
							}
						} else {
							//if update/edit/delete, it must return a row
							if (!requestID.equals("getCustomUserKeywordAlerts")) {
								auditLog(request, javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR, token, user, response.get("Data"));
								//if no result is returned, something went wrong
								throw new WebApplicationException(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
							}
						}
					}

					// Check if the returned resultset has column named code. Throw error based on its value.
					if (requestID.equals("getUsers") || requestID.equals("updateUserDetails")) {
						if (resultList.size() > 0) {
							@SuppressWarnings("unchecked")
							HashMap<Object, Object> row = ((HashMap<Object, Object>) resultList.get(0));
							if (row.containsKey("Code")) {
								switch(Integer.parseInt(row.get("Code").toString())) {
								case 1://invalid token
								case 2://user doesn't exists
									auditLog(request, javax.servlet.http.HttpServletResponse.SC_FORBIDDEN, token, user, response.get("Data"));
									throw new WebApplicationException(javax.servlet.http.HttpServletResponse.SC_FORBIDDEN);
								case 3://not admin
									auditLog(request, javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST, token, user, response.get("Data"));
									throw new WebApplicationException(row.get("Status").toString(), javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST);
								}
							}
						}
					}

					// get new token
					if (token != null) {
						String newToken = getNewToken(token);
						if (returnToken(requestID)) response.put("token", newToken);
					}

					auditLog(request, javax.servlet.http.HttpServletResponse.SC_OK, token, user, response.get("Data"));
					return response;
				}

			} else { // token not provided
				auditLog(request, javax.servlet.http.HttpServletResponse.SC_FORBIDDEN, token, user, null);
				throw new WebApplicationException(javax.servlet.http.HttpServletResponse.SC_FORBIDDEN);
			}

		} else { // other formats not implemented
			auditLog(request, javax.servlet.http.HttpServletResponse.SC_NOT_IMPLEMENTED, token, user, null);
			throw new WebApplicationException(javax.servlet.http.HttpServletResponse.SC_NOT_IMPLEMENTED);
		}

	}

	public static HashMap<String, String> getResponse(String format, HttpServletRequest request, String requestID, String userID) {
		return getResponse(format, (ArrayList<Object>)null, (LinkedHashMap<String,Object>)null, request, 0, requestID);
	}

	public static HashMap<String, String> getResponse(String format, HttpServletRequest request, String requestID) {
		return getResponse(format, (ArrayList<Object>)null, (LinkedHashMap<String,Object>)null, request, 0, requestID);
	}

	public static HashMap<String, String> getResponse(String format, LinkedHashMap<String,Object> sqlParams, HttpServletRequest request, int page, String requestID) {
		return getResponse(format, (ArrayList<Object>)null, sqlParams, request, page, requestID);
	}

	public static HashMap<String, String> getAuthenticatedResponse(String format, LinkedHashMap<String,Object> sqlParams, HttpServletRequest request, int page, String requestID) {
		return getResponse(format, (ArrayList<Object>)null, sqlParams, request, page, requestID);
	}

	public static HashMap<String, String> getResponse(String format, ArrayList<Object> sqlParams, HttpServletRequest request, int page, String requestID) {
		return getResponse(format,sqlParams, (LinkedHashMap<String,Object>)null, request, page, requestID);
	}

	private static boolean requireToken(String requestID) {
		ArrayList<String> IDs = new ArrayList<String>();

		IDs.add("getUserIDDB");
		IDs.add("getCTAS");
		IDs.add("getClassifications");
		IDs.add("getClassifiers");
		IDs.add("getMappingCounts");
		IDs.add("getFSAs");
		IDs.add("getAlertTypes");
		IDs.add("forgotUsername");
		IDs.add("forgotPassword");
		IDs.add("getBuckets");
		IDs.add("getSyndromes");
		IDs.add("getILIPercentage");
		IDs.add("getILIColours");
		IDs.add("getAsthmaPercentage");
		IDs.add("getGeogs");
		IDs.add("getPHOPanam");
		IDs.add("getUserUUIDByUsernameNoToken");
		IDs.add("registerNewUser");
		IDs.add("getILI_Resp_Totals");
		IDs.add("getFlu_totals");
		IDs.add("getILIMapper_Ontario_Colors"); 

		return !(IDs.contains(requestID));
	}

	private static boolean removeTokenParm(String requestID) {
		ArrayList<String> IDs = new ArrayList<String>();

		IDs.add("getAlerts");
		IDs.add("getFSAs"); // remove this item once SPROC removes token parameter
		IDs.add("getAuthHospsAndPhus");
		IDs.add("getUserToken");
		IDs.add("changePassword");
		IDs.add("getVisitDetails");
		IDs.add("getPhuClassVisits");
		IDs.add("getPhuClassAdmissions");
		IDs.add("getClassifiedVisitListings");
		IDs.add("getClassifiedAdmissionListings");
		IDs.add("getAlerts");
		IDs.add("getMappingCounts");
		IDs.add("getCustomUserKeywordAlerts");
		IDs.add("validateCustomUserKeywordAlerts");
		IDs.add("addCustomUserKeywordAlerts");
		IDs.add("updateCustomUserKeywordAlerts");
		IDs.add("updateCustomUserKeywordAlertsActiveStatus");
		IDs.add("deleteCustomUserKeywordAlerts");
		IDs.add("getCustomUserKeywordNotifications");
		IDs.add("getCustomUserKeywordNotificationDetail");
		IDs.add("getUsers");
		IDs.add("getAppSettings");
		IDs.add("updateAppSetting");
		IDs.add("getUserUUIDByUsername");
		IDs.add("createUser");
		IDs.add("updateUserDetails");
		IDs.add("updateUserStatus");
		IDs.add("updateUserArchived");
		IDs.add("getUserResources");
		IDs.add("updateUserResource");
		IDs.add("deleteUserResource");
		IDs.add("addUserResource");
		IDs.add("setPasswordAdmin");

		return !(IDs.contains(requestID));
	}

	private static boolean returnToken(String requestID) {
		ArrayList<String> IDs = new ArrayList<String>();

		IDs.add("logoutUserToken");
		IDs.add("forgotUsername");
		IDs.add("forgotPassword");
		IDs.add("getPHOPanam");

		return !(IDs.contains(requestID));
	}

	public static boolean auditLog(HttpServletRequest request, int responseCode, String response) {
		return auditLog(request, responseCode, null, null, response);
	}

	public static boolean auditLog(HttpServletRequest request, int responseCode, String token, String response) {
		return auditLog(request, responseCode, token, null, response);
	}

	public static boolean auditLog(HttpServletRequest request, int responseCode, LinkedHashMap<String,Object> linkedSqlParams, String response) {
		// get token
		String token = null;
		if (linkedSqlParams!=null) {
			token = (String)linkedSqlParams.get("token");
		}
		return auditLog(request, responseCode, token, null, response);
	}

	public static boolean auditLog(HttpServletRequest request, int responseCode, String token, String user, String response) {

		String method = request.getMethod();
		String URL = request.getRequestURL().toString();
		String userAgent = request.getHeader("User-Agent");
		String remoteAddr = getRemoteAddr(request);

		String queryString = request.getQueryString();
		if (!(queryString==null)) {
			URL += "?" + queryString;
		}

		String contentType = request.getContentType();
		if (contentType==null) {
			contentType = "";
		}

		Long date = new Date().getTime();

		LinkedHashMap<String,Object> sqlParams = new LinkedHashMap<String,Object>();
		sqlParams.put("token", token);
		sqlParams.put("RequestTime", date);
		sqlParams.put("URL", URL);
		sqlParams.put("Method", method);
		sqlParams.put("UserAgent", userAgent);
		sqlParams.put("RemoteAddr", remoteAddr);
		sqlParams.put("ContentType", contentType);
		sqlParams.put("ResponseCode", responseCode);
		sqlParams.put("User", user);
		sqlParams.put("Response", response);

		Request req = reqReference.getRequest("auditLog");
		DatabaseConnection.setData(req.getStoredProcedure(), sqlParams);

		return true;
	}

	private static String getNewToken(String currentToken) {

		LinkedHashMap<String,Object> sqlParams = new LinkedHashMap<String,Object>();
		sqlParams.put("token", currentToken);

		Request req = reqReference.getRequest("getUserToken");
		ResultSet dataResults = DatabaseConnection.getData(req.getStoredProcedure(), sqlParams);

		String token = null;

		ResultSetMetaData rsmd;
		try {
			rsmd = dataResults.getMetaData();

			dataResults.next();
			for (int i = 1; i <= rsmd.getColumnCount(); i++) {
				if (rsmd.getColumnName(i).equals("token")) {
					token = dataResults.getString(i);
					break;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return token;
	}

}
