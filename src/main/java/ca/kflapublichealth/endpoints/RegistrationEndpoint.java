package ca.kflapublichealth.endpoints;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import net.oauth.OAuthException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import ca.kflapublichealth.requests.RequestManager;

@Path("/registration")
public class RegistrationEndpoint {
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/user/useruuid-by-username/{username}")
	public static String getUserUUIDByUsernameNoToken(
			@PathParam("username") String username,
			@DefaultValue("json") @QueryParam("format") String format,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) throws OAuthException, IOException, URISyntaxException {
	
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("username", username);

		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
			request,0, "getUserUUIDByUsernameNoToken");

		return response.get("Data");
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/user/register-new-user")
	public static String registerNewUser(		
			@FormParam("UserName") String UserName,
			@FormParam("Password") String Password,
			@FormParam("Email") String Email,
			@FormParam("FName") String FName,
			@FormParam("LName") String LName,
			@FormParam("Position") String Position,			
			@FormParam("Agency") String Agency,
			@FormParam("Telephone") String Telephone,	
			@FormParam("Recaptcha") String Recaptcha,	
			@DefaultValue("json") @QueryParam("format") String format,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) throws OAuthException, IOException, URISyntaxException {
		
		HttpClient httpclient = HttpClients.createDefault();
		HttpPost httppost = new HttpPost("https://www.google.com/recaptcha/api/siteverify");

		// Request parameters and other properties.
		List<NameValuePair> params = new ArrayList<NameValuePair>(2);
		params.add(new BasicNameValuePair("secret", "6LfdvCUTAAAAABY8btMntYL_V3fCmySqs44TQoV8"));
		params.add(new BasicNameValuePair("response", Recaptcha));
		httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

		try {
			//Execute and get the response.
			HttpResponse response = httpclient.execute(httppost);
			String jsonString = EntityUtils.toString(response.getEntity());			
			JsonObject jsonObject = new Gson().fromJson(jsonString, JsonObject.class);	
			String successStatus = jsonObject.get("success").toString().toLowerCase();

			if (successStatus.equals("true")){
							
				LinkedHashMap<String, Object> rmParams = new LinkedHashMap<String, Object>();
				rmParams.put("UserName", UserName);
				rmParams.put("Password", Password);
				rmParams.put("Email", Email);
				rmParams.put("FName", FName);
				rmParams.put("LName", LName);
				rmParams.put("Position", Position);
				rmParams.put("Agency", Agency);
				rmParams.put("Telephone", Telephone);
				
				HashMap<String, String> rmResponse =  RequestManager.getAuthenticatedResponse(format,rmParams,
					request,0, "registerNewUser");

				//Check response content
				if (!rmResponse.get("Data").isEmpty()){
					//If the new userid is returned
					return "true";
				} else {
					//return is empty
					return "false";
				}								
                                                  
			} else {
				//On site not verified return false
				return "false";
			}
		
		} catch (IOException e) {
			//On error return false
		    return "false";
		}
	}
}
