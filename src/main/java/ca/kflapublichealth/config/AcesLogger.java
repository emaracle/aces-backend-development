package ca.kflapublichealth.config;

import java.io.InputStream;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;

//import org.apache.log4j.PropertyConfigurator;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * Class that loads log4j
 * allowing us to log anything in all the project's classes
 * @author wzaghal
 *
 */
public class AcesLogger {
	static final Logger logger = LogManager.getLogger(AcesLogger.class);

	public static void loadLogger(ServletContext request, String path) {
		InputStream is = request.getResourceAsStream(path);
	        //PropertyConfigurator.configure(is);
	}

}
