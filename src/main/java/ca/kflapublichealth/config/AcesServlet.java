package ca.kflapublichealth.config;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import ca.kflapublichealth.connections.DatabaseConfig;
import ca.kflapublichealth.requests.RequestReference;

import org.glassfish.jersey.servlet.ServletContainer;

/**
 * The initial Servlet that is configured for the AcesEpiEndpoint prjoect. The
 * init file gets executed once deployed on an application server.
 * 
 * @author wzaghal
 * 
 */
public class AcesServlet extends ServletContainer {
	static final Logger logger = LogManager.getLogger(AcesServlet.class);
	static final String ConfigAddress = "/Properties/Config.xml";

	/**
	 * Loads Log4j and the Request Configuration before initialising all the
	 * standard configuration necessary for Java EE.
	 * 
	 * By populating all the requests, all data in the Config.xml is read and
	 * converted to Request Objects so that the project can execute the correct
	 * stored procedures and return data
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {

		AcesLogger.loadLogger(config.getServletContext(), "/Properties/log4j.properties");
		logger.info("Log4j loaded successfully");
		logger.info("populating Requests");

		try {
			RequestReference.populateRequests(config.getServletContext(), ConfigAddress);
		} catch (Exception e) {
			logger.fatal("Request Population unsuccessful");
			logger.fatal(e.toString());
		}

		try {
			logger.info("---- configuring datbase ------");
			DatabaseConfig.populateDatabaseConfig(config.getServletContext());
		} catch (Exception e) {
			logger.fatal("Failed to configure Database");
			logger.fatal(e.toString());
		}
		
		super.init(config);
	} 
}
