package ca.kflapublichealth.endpoints.secure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ca.kflapublichealth.endpoints.EndpointHelper;
import ca.kflapublichealth.requests.RequestAuthenticator;
import ca.kflapublichealth.requests.RequestManager;

@Path("/secure/visits/classified-line-listings")
public class AuthenticatedClassifiedVisitListingsEndpoint {

	@GET
	@Path("/{locality}/phus/{phu}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	public static String getLocalisedPhuLineListings(
			@PathParam("locality") String phuLocality,
			@PathParam("phu") String phu,
			@DefaultValue("json") @QueryParam("format") String format,
			@DefaultValue("All") @QueryParam("gender") String gender,
			@DefaultValue("0") @QueryParam("agefrom") int ageFrom,
			@DefaultValue("200") @QueryParam("ageto") int ageTo,
			@DefaultValue("0") @QueryParam("datefrom") long dateFrom,
			@DefaultValue("0") @QueryParam("dateto") long dateTo,
			@DefaultValue("YYYYYY") @QueryParam("ctas") String ctas,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request,
			@DefaultValue("ME") @QueryParam("classifier") String classifier,
			@DefaultValue("S2014") @QueryParam("classification") String classification,
			@DefaultValue("All") @QueryParam("bucket") String bucket,
			@QueryParam("class") String classID,
			@QueryParam("token") String token,
			@DefaultValue("All")@QueryParam("patient") String patientLocality,
			@DefaultValue("0") @QueryParam("probability") Double probability,
			@DefaultValue("All")@QueryParam("filterByFSA") String fsa,
			@DefaultValue("All")@QueryParam("hospital") String hospital) {
	
		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}
		
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("phu", phu);
		EndpointHelper.addAllParameter("fsa", fsa, params);
		EndpointHelper.addAllParameter("gender", gender, params);
		params.put("ageFrom", ageFrom);
		params.put("ageoTo", ageTo);
		EndpointHelper.addTheDateParameters(dateFrom, dateTo, params);
		EndpointHelper.addTheCtasParams(ctas, params);
		EndpointHelper.addTheLocalityParameters(phuLocality, patientLocality, params);
		params.put("classifier",classifier);
		params.put("classification",classification);
		EndpointHelper.addAllParameter("classID", classID, params);
		params.put("probability",probability);
		params.put("bucket",bucket);
		params.put("hosp", hospital);
		params.put("token",token);

		HashMap<String, String> response = RequestAuthenticator
				.authenticateRequest(format, params, request, token, "getClassifiedVisitListings");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}
		
		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}
}
