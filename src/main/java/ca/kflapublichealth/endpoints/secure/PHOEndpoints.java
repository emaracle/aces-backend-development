package ca.kflapublichealth.endpoints.secure;

import java.util.HashMap;
import java.util.LinkedHashMap;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.Produces;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ca.kflapublichealth.requests.RequestManager;

@Path("/secure/pho")
public class PHOEndpoints {

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("visits")
	public static String getPHOVisits(
			@QueryParam("user") String user,
			@QueryParam("pass") String pass,
			@QueryParam("geog") String geog,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {		

		if (user.toLowerCase().trim().equals("pho") && pass.toLowerCase().trim().equals("smsp2015")) {
			LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
			params.put("GeogType", geog);
			
			HashMap<String, String> response = RequestManager.getResponse("json", params, request, 0, "getPHOPanam");			
			return response.get("Data");
			
		} else {
			RequestManager.auditLog(request, javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED, null, null, null);		
			throw new WebApplicationException(javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED);		
		}
	}
	
}
