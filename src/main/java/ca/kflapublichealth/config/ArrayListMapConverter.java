package ca.kflapublichealth.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * Helper class for Xstream that illustrates how to translate
 * all the endpoint results to XML
 * @author wzaghal
 *
 */
public class ArrayListMapConverter implements Converter {

	@Override
	public boolean canConvert(Class theClass) {
		return theClass.equals(ArrayList.class);
	}

	@Override
	public void marshal(Object obj, HierarchicalStreamWriter writer,
			MarshallingContext arg2) {
		ArrayList<HashMap<String, String>> arrayList = (ArrayList<HashMap<String, String>>) obj;
		
		for (HashMap<String, String> map : arrayList) {
			Iterator it = map.entrySet().iterator();
			writer.startNode("elem");
			while (it.hasNext()) {
				Map.Entry pairs = (Map.Entry) it.next();
				writer.startNode(pairs.getKey().toString());
				writer.setValue(pairs.getValue().toString());
				writer.endNode();
		}
			writer.endNode();
			it.remove(); // avoids a ConcurrentModificationException
		}

	}

	@Override
	public Object unmarshal(HierarchicalStreamReader arg0,
			UnmarshallingContext arg1) {
		return null;
	}

}
