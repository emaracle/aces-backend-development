package ca.kflapublichealth.output;

import java.util.ArrayList;
import java.util.List;
import ca.kflapublichealth.output.Syndrome;

public class Classification {
	public List<Syndrome> ME = new ArrayList<Syndrome>();
	public List<Syndrome> BW = new ArrayList<Syndrome>();
	public List<Syndrome> C45 = new ArrayList<Syndrome>();
	public List<Syndrome> W = new ArrayList<Syndrome>();
	public List<Syndrome> NB = new ArrayList<Syndrome>();
	public List<Syndrome> MCME = new ArrayList<Syndrome>();		
}