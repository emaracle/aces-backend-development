package ca.kflapublichealth.helpers;

public class JiraInitException extends Exception {

    /**
     * For subclasses only.
     */
    protected JiraInitException() {
    }

    /**
     * @param message
     */
    public JiraInitException(String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public JiraInitException(Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public JiraInitException(String message, Throwable cause) {
        super(message, cause);
    }

}
