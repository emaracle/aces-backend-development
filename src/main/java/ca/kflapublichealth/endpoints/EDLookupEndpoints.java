package ca.kflapublichealth.endpoints;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ca.kflapublichealth.requests.RequestManager;

/**
 * Provides the remote service to access and retrieve reference tables that can be used to understand
 * the data that the other endpoints provide.
 * 
 * These are:
 * 
 * Data about classifiers being used, classification schemas and their respective classes
 * Data about Canadian Triage and Accuity Scale
 * 
 * @author wzaghal
 */
@Path("/lookup")
public class EDLookupEndpoints {

	/**
	 * Returns the classification schemas available and classes that correspond
	 * to each classification schema.
	 * 
	 * @param format
	 *            XML or JSON. The return format
	 * @param httpResponse
	 *            internal web parameter
	 * @param request
	 *            internal web parameter
	 * @return XML or JSON classification and class data
	 */
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/classifications")
	public static String getClassififications(
			@DefaultValue("json") @QueryParam("format") String format,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {

		HashMap<String, String> response = RequestManager.getResponse(format,
				request, "getClassifications");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}

	/**
	 * Returns current active classifying algorithms and a description of each
	 * algorithm
	 * 
	 * @param format
	 *            XML or JSON. The return format
	 * @param httpResponse
	 *            internal web parameter
	 * @param request
	 *            internal web parameter
	 * @return XML or JSON classifier data
	 */
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/classifiers")
	public static String getClassifiers(
			@DefaultValue("json") @QueryParam("format") String format,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {

		HashMap<String, String> response = RequestManager.getResponse(format,
				request, "getClassifiers");

		httpResponse.addHeader("Link", response.get("Header"));
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}
		
		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}

	/**
	 * Returns Canadian Triage and Accuity Scale data and description
	 *
	 * @param format
	 *            XML or JSON. The return format
	 * @param httpResponse
	 *            internal web parameter
	 * @param request
	 *            internal web parameter
	 * @return XML or JSON  Canadian Triage and Accuity Scale data and description
	 */
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/ctas")
	public static String getCTAS(
			@DefaultValue("json") @QueryParam("format") String format,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {

		HashMap<String, String> response = RequestManager.getResponse(format,
				request, "getCTAS");

		httpResponse.addHeader("Link", response.get("Header"));
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}
		
		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/fsas")
	public static String getHospssAndPhus(
			@DefaultValue("json") @QueryParam("format") String format,
			@DefaultValue("All") @QueryParam("phu") String phu,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {
			
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("token", ""); // pass empty token until SPROC removes token parameter
		if (phu.equals("All"))
			params.put("phu",null);
		else
		params.put("phu",phu);
		
		HashMap<String, String> response =  RequestManager.getResponse(format, params, request, 0, "getFSAs");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}
		
		httpResponse.addHeader("Link", response.get("Header"));

		return response.get("Data");
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/alerttypes")
	public static String getAlertTypes(
			@DefaultValue("json") @QueryParam("format") String format,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {

		HashMap<String, String> response = RequestManager.getResponse(format, request, "getAlertTypes");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}
		
		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}


	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/buckets")
	public static String getBuckets(
			@DefaultValue("json") @QueryParam("format") String format,
			@DefaultValue("S2014") @QueryParam("classification") String classification,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {

		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("classification", classification);
        
		HashMap<String, String> response =  RequestManager.getResponse(format, params, request, 0, "getBuckets");

		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}


	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/syndromes")
	public static String getSyndromes(
			@DefaultValue("json") @QueryParam("format") String format,
			@DefaultValue("S2014") @QueryParam("classification") String classification,
			@DefaultValue("All") @QueryParam("bucket") String bucket,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {

		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("classification", classification);
        params.put("bucket", bucket);
        
		HashMap<String, String> response =  RequestManager.getResponse(format, params, request, 0, "getSyndromes");

		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}


	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/geogs")
	public static String getGeogs(@DefaultValue("json") @QueryParam("format") String format,
								  @Context HttpServletResponse httpResponse,
								  @Context HttpServletRequest request) {

		HashMap<String, String> response = RequestManager.getResponse(format, request, "getGeogs");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}
		
		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}
	
}
