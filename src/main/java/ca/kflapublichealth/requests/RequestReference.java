package ca.kflapublichealth.requests;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;

import javax.servlet.ServletContext;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import ca.kflapublichealth.config.AcesServlet;

import com.thoughtworks.xstream.XStream;

/**
 * Responsible for the storage and retrieval of an ArrayList of Requests
 * 
 * @author wzaghal
 */
public class RequestReference {
	static final Logger logger = LogManager.getLogger(AcesServlet.class);
	static ArrayList<Request> requestReference;

	/**
	 * Setter method to store the ArrayList containing Requests
	 * 
	 * @param reqList
	 *            ArrayList of Requests
	 */
	public void setRequestList(ArrayList<Request> reqList) {
		requestReference = reqList;
	}

	/**
	 * Getter method to return the ArrayList containing Requests
	 * 
	 * @return ArrayList of Requests
	 */
	public ArrayList<Request> getAllRequests() {
		return requestReference;
	}

	/**
	 * Returns the Request specified by the unique name.
	 * 
	 * @param name
	 *            the unique name of the Request
	 * @return Request that has the specified name
	 */
	public Request getRequest(String name) {
		for (Request req : requestReference) {
			if (req.name.equals(name)) {
				return req;
			}
			if (req.hasChild()) {
				// Check child requests
				if (req.getChild().getName().equals(name)) {
					return req.getChild();
				}
			}
		}

		// TODO - throw error.
		return null;

	}

	/**
	 * Populates all the different Requests the project can handle for future
	 * reference from an XML file.
	 * 
	 * @param context
	 *            the ServletContext
	 * @param path
	 *            the path to the XML File
	 */
	@SuppressWarnings("unchecked")
	public static void populateRequests(ServletContext context, String path)
			throws Exception {

		InputStream is = context.getResourceAsStream(path);
		XStream xstream = new XStream();
		xstream.registerConverter(new RequestConverter());

		requestReference = (ArrayList<Request>) xstream.fromXML(is);

	}

}