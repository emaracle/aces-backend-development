package ca.kflapublichealth.requests;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
/**
 * Processes the request by filtering through the data collected from the
 * database and returning what is required.
 * 
 * @author wzaghal
 * 
 */
public class RequestProcessor {
	static final Logger logger = LogManager.getLogger(RequestProcessor.class);

	/**
	 * Analyzes the resultset and returns the total number of visits
	 * 
	 * @param rs
	 * @return
	 */
	public static ArrayList<Object> getDataFromResultSet(ResultSet rs) {
		ResultSetMetaData rsmd;
		ArrayList<Object> resultsList = new ArrayList<Object>();
		HashMap<Object, Object> visits = new HashMap<Object, Object>();

		try {
			rsmd = rs.getMetaData();

			while (rs.next()) {
				for (int i = 1; i <= rsmd.getColumnCount(); i++) {
					// ensure all data types are formatted correctly
					// add to HashMap
					

					if ((rsmd.getColumnTypeName(i).equals("datetime")||rsmd.getColumnTypeName(i).equals("date")) && rs.getTimestamp(i) != null) {
					//	logger.debug("Putting " + rs.getTimestamp(i) + " into " + rsmd.getColumnName(i));
						visits.put(rsmd.getColumnName(i), rs.getTimestamp(i)
								.getTime());

					} else if (rsmd.getColumnTypeName(i).equals("int")) {
					//	logger.debug("Putting " + rs.getInt(i) + " into " + rsmd.getColumnName(i));
						visits.put(rsmd.getColumnName(i), rs.getInt(i));
					} else {
					//	logger.debug("Putting " + rs.getString(i) + " into " + rsmd.getColumnName(i));
						visits.put(rsmd.getColumnName(i), rs.getString(i));
					}
				}
				resultsList.add(visits.clone());
				visits.clear();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultsList;
	}
	

}
