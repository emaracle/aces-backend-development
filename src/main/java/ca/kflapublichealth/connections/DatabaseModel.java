package ca.kflapublichealth.connections;

public class DatabaseModel {
	String driver;
	String host;
	String dbName;
	String user;
	String pw;
	
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver= driver;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	} 
	public String getConnString(){
		return "jdbc:sqlserver://"+this.host+":1433;DatabaseName="+this.dbName;
	}
}
