package ca.kflapublichealth.endpoints.secure;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.Produces;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import ca.kflapublichealth.endpoints.EndpointHelper;
import ca.kflapublichealth.requests.RequestAuthenticator;
import ca.kflapublichealth.requests.RequestManager;

@Path("/secure/visits")
public class AuthenticatedEDVisitsEndpoints {

	/**
	 * Provides the remote service for accessing and retrieving EDVisits whilst
	 * specifying a combination of parameters to make the data as customisable
	 * and specific as possible.
	 * 
	 * @author wzaghal
	 */
	static final Logger logger = LogManager.getLogger(AuthenticatedEDVisitsEndpoints.class);

	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("{locality}/phus/{phu}")
	public static String getLocalityVisitsForPhu(
			@PathParam("locality") String phuLocality,
			@PathParam("phu") String phu,
			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("gender") String gender,
			@DefaultValue("0") @QueryParam("agefrom") int ageFrom,
			@DefaultValue("200") @QueryParam("ageto") int ageTo,
			@DefaultValue("0") @QueryParam("datefrom") long dateFrom,
			@DefaultValue("0") @QueryParam("dateto") long dateTo,
			@DefaultValue("YYYYYY") @QueryParam("ctas") String ctas,
			@DefaultValue("local") @QueryParam("patient") String patientLocality,
			@QueryParam("token") String token,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {
	
		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}

		logger.info("\nRequest by:\t" + request.getLocalAddr() + "\nlocality: "
				+ phuLocality + "patientLocality: " + patientLocality
				+ "\nphu: " + phu + "\nformat: " + format + "\ngender: "
				+ gender + "\nageFrom: " + ageFrom + "\nageTo: " + ageTo
				+ "\ndateFrom: " + dateFrom + "\ndateTo: " + dateTo
				+ "\nctas: " + ctas);

	
		
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("phu", phu);
		params.put("hosp", null); 
		params.put("gender", gender);
		params.put("ageFrom", ageFrom);
		params.put("ageoTo", ageTo);
		EndpointHelper.addTheDateParameters(dateFrom, dateTo, params);
		EndpointHelper.addTheCtasParams(ctas, params);
		EndpointHelper.addTheLocalityParameters(phuLocality, patientLocality, params);
		params.put("token", token);

		HashMap<String, String> response = RequestAuthenticator
				.authenticateRequest(format, params, request, token,"getGeoTotalVisitsPerPHU");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		return response.get("Data");
	}


	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("alerts/{VisitID}")
	public static String getVisitAlerts(
			@PathParam("VisitID") String VisitID,
			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("token") String token,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {
	
		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}

		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("VisitID", VisitID);
		params.put("token", token);

		HashMap<String, String> response = RequestManager.getResponse(format, params, request, 0, "getVisitAlerts");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		return response.get("Data");
	}


	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("{VisitID}")
	public static String getVisitDetails(
			@PathParam("VisitID") String VisitID,
			@DefaultValue("ed") @QueryParam("visitType") String visitType,
			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("token") String token,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {
	
		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}

		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("VisitID", VisitID);
		params.put("token", token);
		params.put("visitType", visitType);

		HashMap<String, String> response = RequestManager.getResponse(format, params, request, 0, "getVisitDetails");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		return response.get("Data");
	}
}
