package ca.kflapublichealth.connections;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map; 

import javax.naming.InitialContext;
import javax.sql.DataSource;
import javax.sql.rowset.CachedRowSet;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import ca.kflapublichealth.requests.RequestAuthenticator;
import ca.kflapublichealth.requests.RequestProcessor;

import com.sun.rowset.CachedRowSetImpl;

/**
 * Class that connects to the database to return data.
 * 
 * @author wzaghal
 * 
 */
public class DatabaseConnection {

	static final Logger logger = LogManager.getLogger(DatabaseConnection.class); 
	static final DatabaseConfig databaseConfig = new DatabaseConfig();
	/**
	 * Perform set operations that do not require a return result set.
	 *
	 * @param storedProcedureName
	 * @param sqlParams
	 * @return
	 */
	public static void setData( String storedProcedureName, LinkedHashMap<String,Object> sqlParams ) {
		// Init SQL connection parameters
		ResultSet rs = null;
		CallableStatement cstmt = null;
		Connection conn = null;
		CachedRowSet cachedRs = null;

		try {
			// Connect 
			
			DatabaseModel databaseModel = databaseConfig.getDatabaseModel();
			Class.forName(databaseModel.getDriver());
			conn = DriverManager.getConnection(databaseModel.getConnString(), databaseModel.getUser(), databaseModel.getPw()); 
			
			String paramHolder = "";
			for (int i = 0; i < sqlParams.size(); i++) {
				paramHolder += "?";
				if (i != sqlParams.size() - 1) {
					paramHolder += ",";
				}

			}
			cstmt = conn.prepareCall("{call " + storedProcedureName + "("+paramHolder+")}");

			// Passing SQL Parameters
			int i = 1;
			Object param = null;
			Iterator it = sqlParams.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pairs = (Map.Entry) it.next();
				param = pairs.getValue();

				if (param instanceof String) {
					cstmt.setString(i, (String) param);
				} else if (param instanceof Integer) {
					logger.debug("Int to var " + i + ", value: " + param);

					cstmt.setInt(i, (Integer) param);
				}else if (param instanceof Boolean) {
					cstmt.setBoolean(i,(Boolean) param);
					logger.debug("Bool to var " + i + ", value: " + param);


				}else if (param instanceof Long) {
					cstmt.setTimestamp(i, new Timestamp((Long) param));
					logger.debug("Long to var " + i + ", value: " + param);


				}else if (param instanceof Double) {
					cstmt.setDouble(i, (Double)param);
					logger.debug("Double to var " + i + ", value: " + param);


				} else if (param == null) {
					logger.debug("Null to var " + i);

					// Only allowing Nvarchar nulls
					cstmt.setNull(i, java.sql.Types.NVARCHAR);
				}else{
					logger.debug("Other to var " + i + ", value: " + param);
				}
				i++;
				it.remove();
			}

			cstmt.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeAll(null, cstmt, conn);
		}
	}

	public static ResultSet getData(String storedProcedureName,
			LinkedHashMap<String,Object> sqlParams) {
		// Init SQL connection parameters
		ResultSet rs = null;
		CallableStatement cstmt = null;
		Connection conn = null;
		CachedRowSet cachedRs = null;

		try {
			// Connect

			DatabaseModel databaseModel = databaseConfig.getDatabaseModel();
			Class.forName(databaseModel.getDriver());
			conn = DriverManager.getConnection(databaseModel.getConnString(), databaseModel.getUser(), databaseModel.getPw()); 
			
			String paramHolder = "";
			for (int i = 0; i < sqlParams.size(); i++) {
				paramHolder += "?";
				if (i != sqlParams.size() - 1) {
					paramHolder += ",";
				}

			}
			//Test cstmt = conn.prepareCall("{exec " + storedProcedureName
			//		+ paramHolder);
			cstmt = conn.prepareCall("{call " + storedProcedureName + "("+paramHolder+")}");
			

			// Passing SQL Parameters
				int i = 1;
				Object param = null;
				Iterator it = sqlParams.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry pairs = (Map.Entry) it.next();
					param = pairs.getValue();
				if (param instanceof String) {
					cstmt.setString(i, (String) param);
				} else if (param instanceof Integer) {
					logger.debug("Int to var " + i + ", value: " + param);

					cstmt.setInt(i, (Integer) param);
				}else if (param instanceof Boolean) {
					cstmt.setBoolean(i,(Boolean) param);
					logger.debug("Bool to var " + i + ", value: " + param);

					
				}else if (param instanceof Long) {
					cstmt.setTimestamp(i, new Timestamp((Long) param));
					logger.debug("Long to var " + i + ", value: " + param);


				}else if (param instanceof Double) {
					cstmt.setDouble(i, (Double)param);
					logger.debug("Double to var " + i + ", value: " + param);


				} else if (param == null) {
					logger.debug("Null to var " + i);

					// Only allowing Nvarchar nulls
					cstmt.setNull(i, java.sql.Types.NVARCHAR);
				}else{
					logger.debug("Other to var " + i + ", value: " + param);
				}
				i++;
				it.remove();
			}

			cstmt.execute();
			
			rs = cstmt.getResultSet();
			
			cachedRs = new CachedRowSetImpl();
			if(rs != null) {
				cachedRs.populate(rs);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeAll(rs, cstmt, conn);
		}
		return cachedRs;
	}
	/**
	 */
	
	
	/**
	 */ 
	public static ResultSet getData(String storedProcedureName,
			ArrayList<Object> sqlParams) {
		// Init SQL connection parameters
		ResultSet rs = null;
		CallableStatement cstmt = null;
		Connection conn = null;
		CachedRowSet cachedRs = null;

		try {
			// Connect
			
			DatabaseModel databaseModel = databaseConfig.getDatabaseModel();
			Class.forName(databaseModel.getDriver());
			conn = DriverManager.getConnection(databaseModel.getConnString(), databaseModel.getUser(), databaseModel.getPw()); 
			
			String paramHolder = "";
			for (int i = 0; i < sqlParams.size(); i++) {
				paramHolder += "?";
				if (i != sqlParams.size() - 1) {
					paramHolder += ",";
				}

			}
			logger.debug(paramHolder);
			//Test cstmt = conn.prepareCall("{exec " + storedProcedureName
			//		+ paramHolder);
			cstmt = conn.prepareCall("{call " + storedProcedureName + "("+paramHolder+")}");
			int i = 1;

			// Passing SQL Parameters
			for (Object param : sqlParams) {
				if (param instanceof String) {
					cstmt.setString(i, (String) param);
				} else if (param instanceof Integer) {
					logger.debug("Int to var " + i + ", value: " + param);

					cstmt.setInt(i, (Integer) param);
				}else if (param instanceof Boolean) {
					cstmt.setBoolean(i,(Boolean) param);
					logger.debug("Bool to var " + i + ", value: " + param);

					
				}else if (param instanceof Long) {
					cstmt.setTimestamp(i, new Timestamp((Long) param));
					logger.debug("Long to var " + i + ", value: " + param);


				}else if (param instanceof Double) {
					cstmt.setDouble(i, (Double)param);
					logger.debug("Double to var " + i + ", value: " + param);


				} else if (param == null) {
					logger.debug("Null to var " + i);

					// Only allowing Nvarchar nulls
					cstmt.setNull(i, java.sql.Types.NVARCHAR);
				}else{
					logger.debug("Other to var " + i + ", value: " + param);
				}
				i++;
			}

			cstmt.execute();
			
			rs = cstmt.getResultSet();
			
			cachedRs = new CachedRowSetImpl();
			if(rs != null) {
				cachedRs.populate(rs);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeAll(rs, cstmt, conn);
		}
		return cachedRs;
	}

	public static ResultSet getSize(String storedProcedureName) {
		ResultSet rs = null;
		CallableStatement cstmt = null;
		Connection conn = null;
		CachedRowSet cachedRs = null;

		try { 

			DatabaseModel databaseModel = databaseConfig.getDatabaseModel();
			Class.forName(databaseModel.getDriver());
			conn = DriverManager.getConnection(databaseModel.getConnString(), databaseModel.getUser(), databaseModel.getPw()); 
			
			cstmt = conn.prepareCall("{call " + storedProcedureName + "}");
			cstmt.execute();
			rs = cstmt.getResultSet();
			cachedRs = new CachedRowSetImpl();
			if(rs != null) {
				cachedRs.populate(rs);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeAll(rs, cstmt, conn);
		}
		return cachedRs;
	}


	public static ResultSet getData(String storedProcedureName) {
		ResultSet rs = null;
		CallableStatement cstmt = null;
		Connection conn = null;
		CachedRowSet cachedRs = null;

		try {
			DatabaseModel databaseModel = databaseConfig.getDatabaseModel();
			Class.forName(databaseModel.getDriver());
			conn = DriverManager.getConnection(databaseModel.getConnString(), databaseModel.getUser(), databaseModel.getPw()); 
			
			cstmt = conn.prepareCall("{call " + storedProcedureName + "}");

			cstmt.execute();
			rs = cstmt.getResultSet();
			cachedRs = new CachedRowSetImpl();
			if(rs != null) {
				cachedRs.populate(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeAll(rs, cstmt, conn);
		}
		return cachedRs;
	}

	/**
	 * Helper class that closes all variables that were opened to gather data
	 * from the database.
	 * 
	 * @param resultSet
	 *            The resultSet created to access from the database
	 * @param statement
	 *            The statement used on the database
	 * @param connection
	 *            The connection used to connect to the database.
	 */
	private static void closeAll(ResultSet resultSet, Statement statement,
			Connection connection) {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
			}
		}
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
			}
		}
	}


}
