package ca.kflapublichealth.endpoints.secure;

import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import ca.kflapublichealth.requests.RequestManager;

@Path("/secure/custom-user-keyword-alerts/")
public class AuthenticatedCustomUserKeywordAlertsEndpoint {
	static final Logger logger = LogManager.getLogger(AuthenticatedCustomUserKeywordAlertsEndpoint.class);
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/{userid}")
	public static String get(
			@DefaultValue("json") @QueryParam("format") String format,
			@PathParam("userid") String userid,
			@QueryParam("token") String token,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {
	
		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}
		
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		params.put("userid", userid);
		
		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "getCustomUserKeywordAlerts");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}
		
		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/{userid}/add")
	public static String add(
			@DefaultValue("json") @QueryParam("format") String format,
			@PathParam("userid") String userid,
			@QueryParam("token") String token,
			@FormParam("keyword") String keyword,
			@FormParam("period") int period,
			@FormParam("threshold") int threshold,
			@FormParam("phu") String phu,
			@FormParam("hospitalid") String hospitalid,
			@FormParam("visittype") String visittype,
			@FormParam("keyworddesc") String keyworddesc,
			
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {
	
		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}
		
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		params.put("userid", userid);
		params.put("keyword", keyword);
		params.put("period", period);
		params.put("threshold", threshold);
		params.put("phu", phu);
		params.put("hospitalid", hospitalid);
		params.put("visittype", visittype);
		params.put("keyworddesc", keyworddesc);
		
		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "addCustomUserKeywordAlerts");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}
		
		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/{userid}/update/{keyworduuid}/")
	public static String update(
			@DefaultValue("json") @QueryParam("format") String format,
			@PathParam("userid") String userid,
			@PathParam("keyworduuid") String keyworduuid,
			@QueryParam("token") String token,
			@FormParam("keyword") String keyword,
			@FormParam("period") int period,
			@FormParam("threshold") int threshold,
			@FormParam("phu") String phu,
			@FormParam("hospitalid") String hospitalid,
			@FormParam("visittype") String visittype,
			@FormParam("keyworddesc") String keyworddesc,
			
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {
	
		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}
		
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		params.put("userid", userid);
		params.put("keyworduuid", keyworduuid);
		params.put("keyword", keyword);
		params.put("period", period);
		params.put("threshold", threshold);
		params.put("phu", phu);
		params.put("hospitalid", hospitalid);
		params.put("visittype", visittype);
		params.put("keyworddesc", keyworddesc);
		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "updateCustomUserKeywordAlerts");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}
		
		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/{userid}/updateActiveStatus/{keyworduuid}/")
	public static String update(
			@DefaultValue("json") @QueryParam("format") String format,
			@PathParam("userid") String userid,
			@PathParam("keyworduuid") String keyworduuid,
			@QueryParam("token") String token,
			@FormParam("active") Boolean active,
					
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {
	
		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}

		System.out.println("Keyword:" + keyworduuid);
		System.out.println("Status:" + active);
		
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		params.put("userid", userid);
		params.put("keyworduuid", keyworduuid);
		params.put("active", active);
		
		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "updateCustomUserKeywordAlertsActiveStatus");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}
		
		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/{userid}/delete/{keyworduuid}/")
	public static String delete(
			@DefaultValue("json") @QueryParam("format") String format,
			@PathParam("userid") String userid,
			@PathParam("keyworduuid") String keyworduuid,
			@QueryParam("token") String token,
		
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {
	
		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}
		
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		params.put("userid", userid);
		params.put("keyworduuid", keyworduuid);
		
		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "deleteCustomUserKeywordAlerts");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}
		
		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/notifications/{keyworduuid}/")
	public static String getNotifications(
			@DefaultValue("json") @QueryParam("format") String format,
			@PathParam("keyworduuid") String keyworduuid,
			@QueryParam("token") String token,			
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {
	
		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}
		
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		params.put("keyworduuid", keyworduuid);
		
		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "getCustomUserKeywordNotifications");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}
		
		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/notification/{alertuuid}/")
	public static String getNotification(
			@DefaultValue("json") @QueryParam("format") String format,
			@PathParam("alertuuid") String alertuuid,
			@QueryParam("token") String token,			
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {
	
		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}
		
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		params.put("alertuuid", alertuuid);
		
		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "getCustomUserKeywordNotificationDetail");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}
		
		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/validate")
	public static String validate(
			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("token") String token,
			@FormParam("userid") String userid,
			@FormParam("keyword") String keyword,			
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {
	
		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}
		
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		params.put("userid", userid);
		params.put("keyword", keyword);
		
		HashMap<String, String> response = RequestManager.getAuthenticatedResponse(format,params,
				request,0, "validateCustomUserKeywordAlerts");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}
		
		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}
}
