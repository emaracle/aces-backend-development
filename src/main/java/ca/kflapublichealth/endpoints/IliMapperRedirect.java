package ca.kflapublichealth.endpoints;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;

public class IliMapperRedirect extends HttpServlet{

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//@todo move it to properties file
		String redirectURL = "http://mapper.kflaphi.ca/ilimapper";
		// Set response content type
		response.setContentType("text/html");
		response.setStatus(response.SC_MOVED_PERMANENTLY);
		response.setHeader("Location", redirectURL); 
	}
} 