## READ ME ##

This project was started in order to separate the backend from frontend for ACESEpiEndpoints. This helps speed up development as making front-end or back-end changes no longer requires a rebuilding the VM. 

This repo is for testing only. All modificaitons will need to be added to the ACESEpiEndpoints project and tested until further notice.   

**Requirements**

JavaSE-1.7
STS 3.7.3.RELEASE was used
Glassfish 4

## Getting Started ##

- Clone ACESEpiEndpoints to C:/sts-workspace/ACESEpiEndpoints
- Clone this repo to C:/sts-workspace/aces-backend
- Open command line and go to C:/sts-workspace/aces-backend
- run copySource.bat  
- Open the workspace 'sts-workspace' in Eclipse   
- Right click on project -> Maven -> Update Project -> (check) Force Update if Snapshot/Releases -> Ok

*NOTE: If there's missing dependencies from any recent updates to ACESEpiEndpoints then you'll need to add them to the POM 

## NOTES ##

** ALL MODIFICATIONS **

Any changes you make in this project will ultimately need to be added back to the ACESEpiEndpoints project and tested. 
Again this repo is only for testing purposes only until further notice. 

**Context Root** 

The context root of this project is empty ('/'). This allows for development with the ACES Frontend Devlopment project.

** Source Lookup Paths **

If dubgger is unable to find file at break point then the lookup path may need to be set. 

*Run -> debug configurations -> (your glassfish server) -> Source (tab)-> Add -> Java Project -> (your aces project)*

** POM file **

If any dependencies need to be added, please push these changes so the next time we pull this repo the dependencies will be set.