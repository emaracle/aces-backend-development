package ca.kflapublichealth.output;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import ca.kflapublichealth.config.ArrayListMapConverter;
import ca.kflapublichealth.output.Classification;
import ca.kflapublichealth.output.Syndrome;
import ca.kflapublichealth.output.Settings;

import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.CompactWriter;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * Provides local services with the different outputs for the results that have
 * been processed.
 * 
 * @author wzaghal
 * 
 */ 

public class OutputFormat {

	/**
	 * Returns the data in JSON
	 * 
	 * @param data
	 *            the arrayList containing the results that need to be returned
	 * @return Json-formatted data
	 */
	
	public static boolean isJSONValid(String testString) {
    	try {
    		Gson gson = new Gson();
        	gson.fromJson(testString, Object.class);
          	return true;
      	} catch(com.google.gson.JsonSyntaxException ex) { 
        	return false;
      	}
  	}

	public static String toJson(ArrayList<Object> data) {
		Gson gson = new Gson();
		return gson.toJson(data);
	}
	
	public static String toJsonVisitDetail(ArrayList<Object> data) {
		String syndromesStr = null;
		String probabilitiesStr = null;
		
		for (int i = 0; i < data.size(); i++) {
			HashMap<String, Object> item = (HashMap)data.get(i);

			if (item.containsKey("Syndrome")) syndromesStr = (String)item.get("Syndrome");
			if (item.containsKey("SyndromeProbability")) probabilitiesStr = (String)item.get("SyndromeProbability");
			item.remove("Syndrome");
			item.remove("SyndromeProbability");
		}
			
		ArrayList<String> syndromes = new ArrayList<String>(Arrays.asList(syndromesStr.split("\\|")));
		ArrayList<String> probabilities = new ArrayList<String>(Arrays.asList(probabilitiesStr.split("\\|")));
		
		classifications items = new classifications();
		
		for (int x = 0; x < syndromes.size(); x++) {
			ArrayList<String> details = new ArrayList<String>(Arrays.asList(syndromes.get(x).split("\\&")));
			
			if (details.get(0).trim().toUpperCase().equals("S2014")) {
				if (items.S2014 == null) items.S2014 = new Classification();
				switch (details.get(1).trim().toUpperCase()) {
					case "BW":
						items.S2014.BW.add(new Syndrome(details.get(2).trim(), Double.parseDouble(probabilities.get(x))));
						break;
					case "C45":
						items.S2014.C45.add(new Syndrome(details.get(2).trim(), Double.parseDouble(probabilities.get(x))));
						break;
					case "ME": 
						items.S2014.ME.add(new Syndrome(details.get(2).trim(), Double.parseDouble(probabilities.get(x))));
						break;
					case "NB":
						items.S2014.NB.add(new Syndrome(details.get(2).trim(), Double.parseDouble(probabilities.get(x))));
						break;
					case "W":
						items.S2014.W.add(new Syndrome(details.get(2).trim(), Double.parseDouble(probabilities.get(x))));
						break;
					case "MCME":
						items.S2014.MCME.add(new Syndrome(details.get(2).trim(), Double.parseDouble(probabilities.get(x))));
						break;
				}
			} else if (details.get(0).trim().toUpperCase().equals("S2018")){
				if (items.S2018 == null) items.S2018 = new Classification();
				switch (details.get(1).trim().toUpperCase()) {
					case "BW":
						items.S2018.BW.add(new Syndrome(details.get(2).trim(), Double.parseDouble(probabilities.get(x))));
						break;
					case "C45":
						items.S2018.C45.add(new Syndrome(details.get(2).trim(), Double.parseDouble(probabilities.get(x))));
						break;
					case "ME": 
						items.S2018.ME.add(new Syndrome(details.get(2).trim(), Double.parseDouble(probabilities.get(x))));
						break;
					case "NB":
						items.S2018.NB.add(new Syndrome(details.get(2).trim(), Double.parseDouble(probabilities.get(x))));
						break;
					case "W":
						items.S2018.W.add(new Syndrome(details.get(2).trim(), Double.parseDouble(probabilities.get(x))));
						break;
					case "MCME":
						items.S2018.MCME.add(new Syndrome(details.get(2).trim(), Double.parseDouble(probabilities.get(x))));
						break;
				}
			}
		}
		
		data.add(items);
		
		Gson gson = new Gson();
		return gson.toJson(data);
	}

	/**
	 * Returns the data in XML
	 * 
	 * @param data
	 *            the arrayList containing the results that need to be returned
	 * @return XML-formatted data
	 */
	public static String toXml(ArrayList<Object> data) {
		XStream xStream = new XStream(new DomDriver());

		// xStream 'learns' how to convert the arrayList<Object> to
		// xml by looking at the ArrayListMapConverter
		xStream.registerConverter(new ArrayListMapConverter());
		xStream.toXML(data);
		return xStream.toXML(data);

	}

	public static String jsonToXml(String strJson){
		// create xstream object for reading xml
		XStream xstream = new XStream(new DomDriver());
		xstream.setMode(XStream.NO_REFERENCES);
    	xstream.alias("settings", Settings.class);

    	// create serialized json to pass to xStream
		Gson gson = new Gson();

		// create a string writer to output to xml (without pretty_printing)
		StringWriter strWriter = new StringWriter();
		xstream.marshal(gson.fromJson(strJson, Settings.class),  new CompactWriter(strWriter));
		String strXML = strWriter.toString();

		return strXML;
	}

	public static String xmlToJson(String strXml){
		// create xstream object for reading xml
		XStream xstream = new XStream(new DomDriver());
		xstream.setMode(XStream.NO_REFERENCES);
    	xstream.alias("settings", Settings.class);

    	// create serialized json from xStream
		Gson gson = new Gson();

		// create a json string 
		String strJson = gson.toJson(xstream.fromXML(strXml));

		return strJson;
	}

}
