package ca.kflapublichealth.endpoints.secure;
import java.util.LinkedHashMap;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.JsonObject;

import ca.kflapublichealth.connections.DatabaseConnection;
import ca.kflapublichealth.helpers.Jira;
import ca.kflapublichealth.requests.Request;
import ca.kflapublichealth.requests.RequestManager;
import ca.kflapublichealth.requests.RequestReference;
import net.oauth.OAuthException;

@Path("/secure/help/")
public class AuthenticatedHelpEndpoint {
	public final static String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.S";
	private static RequestReference reqReference = new RequestReference();
		
	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/create-ticket")
	public static String createTicket(
			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("token") String token,
			@FormParam("category") String category,
			@FormParam("summary") String summary,
			@FormParam("description") String description,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) throws OAuthException, IOException, URISyntaxException {
	
		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}	
		
		String strResponse;
		
		//get user name and email
		LinkedHashMap<String, Object> sqlParams = new LinkedHashMap<String, Object>();
		sqlParams.put("token", token);
		Request dbRequest = reqReference.getRequest("getUserToken");
		ResultSet result = DatabaseConnection.getData(dbRequest.getStoredProcedure(), sqlParams);
		try {
			if (result.next()) {
				
				//HashMap<String, String> response = RequestManager.getResponse(format, sqlParams, request, 0, "getUserToken");
				httpResponse.addHeader("token", result.getString("token"));
				
				String name = result.getString("name");
				String email = result.getString("email");
				
				strResponse = Jira.createTicket(category, summary, description, email, name);
		
			} else {
				throw new Exception("Invalid token");
			}

		} catch (SQLException e) {
			e.printStackTrace();
			JsonObject o = new JsonObject();
			o.addProperty("error", "Error occurred while validating user token.");
			strResponse = o.toString();
		} catch (Exception e) {
			//e.printStackTrace();
			JsonObject o = new JsonObject();
			o.addProperty("error", e.getMessage());
			strResponse = o.toString();
		}
		RequestManager.auditLog(request, javax.servlet.http.HttpServletResponse.SC_OK, token, strResponse);
		return strResponse;
	}


	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/generate-request-token")
	public static String generateRequestToken(
			@DefaultValue("json") @QueryParam("format") String format,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) throws OAuthException, IOException, URISyntaxException {
	
		JsonObject o = new JsonObject();
        try {
        	
        	Map<String, String> map = Jira.getRequestToken();
	        o.addProperty("authorize_url", map.get("authorize_url"));
        	o.addProperty("access_token_url", "/secure/help/refresh-access-token?" + map.get("response_body"));
        	
        } catch (Exception e) {
			o.addProperty("error", e.getMessage());
			e.printStackTrace();
			
		}
        RequestManager.auditLog(request, javax.servlet.http.HttpServletResponse.SC_OK, o.toString());
        return o.toString();
	}
	

	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/refresh-access-token")
	public static String refreshAccessToken(
			@DefaultValue("json") @QueryParam("format") String format,
			@DefaultValue("") @QueryParam("oauth_token") String token,
			@DefaultValue("") @QueryParam("oauth_token_secret") String secret,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) throws OAuthException, IOException, URISyntaxException {
		
		JsonObject o = new JsonObject();
        try {
	        if (Jira.refreshAccessToken(token, secret)) {
	        	o.addProperty("success", true);
	        } else {
	        	o.addProperty("error", "Failed to update.");
	        }
	        
        } catch (Exception e) {
			o.addProperty("error", e.getMessage());
			e.printStackTrace();
		}
        RequestManager.auditLog(request, javax.servlet.http.HttpServletResponse.SC_OK, o.toString());
        return o.toString();
	}
	

	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/refresh-settings")
	public static String refreshSettings(
			@DefaultValue("json") @QueryParam("format") String format,
			@Context HttpServletRequest request) throws OAuthException, IOException, URISyntaxException {
		
		JsonObject o = new JsonObject();
        try {
	        Jira.init(true);
	        o.addProperty("success", true);
        } catch (Exception e) {
			o.addProperty("error", e.getMessage());
			e.printStackTrace();
		}
        RequestManager.auditLog(request, javax.servlet.http.HttpServletResponse.SC_OK, o.toString());
        return o.toString();
	}
	
	
}
