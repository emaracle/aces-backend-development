package ca.kflapublichealth.requests;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
/**
 * A Request is an instruction from the user to provide information. A Request
 * Object holds all the properties of a particular Request such as the name,
 * description and a reference on how to provide that request to the user.
 * 
 * @author wzaghal
 * 
 */
public class Request {
	static final Logger logger = LogManager.getLogger(Request.class);
	String name;
	String description;
	String storedProcedure;
	String paramType;
	String paramDescription;
	Request child = null;

	/**
	 * Returns the Request name
	 * 
	 * @return Request name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Stores the Request name
	 * 
	 * @param name
	 *            Request Name
	 */

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the Request Description
	 * 
	 * @return Request Description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Stores the Request Description
	 * 
	 * @param name
	 *            Request Name
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Returns the Request Stored Procedure
	 * 
	 * @return Request Stored Procedure
	 */
	public String getStoredProcedure() {
		return storedProcedure;
	}

	public void setStoredProcedure(String storedProcedure) {
		this.storedProcedure = storedProcedure;
	}

	/**
	 *Returns the Request child, if the Request has a child.
	 * 
	 * @return Request child
	 */
	public Request getChild() {
		if (child != null) {
			return child;
		}

		// TODO
		return null;
	}

	/**
	 * Stores the Request child.
	 * 
	 * @param req
	 *            A Request object
	 */
	public void setChild(Request req) {

		this.child = req;
	}

	/**
	 * Checks if Request has a child or not
	 * 
	 * @return True if Request exists.
	 */
	public boolean hasChild() {
		if (child == null) {
			return false;
		}
		return true;
	}

	/**
	 * Creates and returns a copy of the Request Object.
	 */
	@Override
	public Request clone() {
		try {
			Request req = new Request();
			req.name = this.name;
			req.description = this.description;
			req.storedProcedure = this.storedProcedure;
			req.child = this.child;
			return req;

		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Clears and removes all associated variables from a Request Object.
	 */
	public void clear() {
		setName(null);
		setDescription(null);
		setStoredProcedure(null);
		setChild(null);
	}
}
