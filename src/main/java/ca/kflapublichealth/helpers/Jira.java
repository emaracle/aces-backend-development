package ca.kflapublichealth.helpers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.base.Splitter;
import com.google.gson.JsonObject;

import net.oauth.OAuth;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;
import net.oauth.OAuthException;
import net.oauth.OAuthMessage;
import net.oauth.ParameterStyle;
import net.oauth.client.OAuthClient;
import net.oauth.client.httpclient4.HttpClient4;
import net.oauth.http.HttpMessage;
import net.oauth.signature.RSA_SHA1;

public class Jira {
	private static boolean initialized = false; 
	
	private static OAuthAccessor accessor;
	
	private static String CONSUMER_KEY;
	private static String PRIVATE_KEY;
	private static String ACCESS_TOKEN;
	private static String BASE_URL;
	private static String PROJECT_KEY;

	public static void init(boolean reload) throws JiraInitException {
		if (!initialized || reload) {
			accessor = null;
			
			//create map of all the setting keys
			//value will be used to check if it's value has been initialized
			ArrayList<String> settingKeys = new ArrayList<>(); 
			settingKeys.add("JIRA_CONSUMER_KEY");
			settingKeys.add("JIRA_PRIVATE_KEY");
			settingKeys.add("JIRA_ACCESS_TOKEN");
			settingKeys.add("JIRA_BASE_URL");
			settingKeys.add("JIRA_PROJECT_KEY");
			
			
			HashMap<String, String> resultValues;
			try {
				resultValues = AcesSetting.getSettings(settingKeys);
			} catch (SQLException e) {
				e.printStackTrace();
				throw new JiraInitException(e.getSQLState());
			}
			
			//make sure all the keys got value
			for(Entry<String, String> entry: resultValues.entrySet()) {
				switch (entry.getKey()) {
				case "JIRA_CONSUMER_KEY":
					if (entry.getValue() == null) throw new JiraInitException(entry.getKey() + " value not initialized.");
					CONSUMER_KEY = entry.getValue();
				break;
				case "JIRA_PRIVATE_KEY":
					if (entry.getValue() == null) throw new JiraInitException(entry.getKey() + " value not initialized.");
					PRIVATE_KEY = entry.getValue();
				break;
				case "JIRA_ACCESS_TOKEN":
					if (entry.getValue() == null) throw new JiraInitException(entry.getKey() + " value not initialized.");
					ACCESS_TOKEN = entry.getValue();
				break;
				case "JIRA_BASE_URL":
					if (entry.getValue() == null) throw new JiraInitException(entry.getKey() + " value not initialized.");
					BASE_URL = entry.getValue();
				break;
				case "JIRA_PROJECT_KEY":
					if (entry.getValue() == null) throw new JiraInitException(entry.getKey() + " value not initialized.");
					PROJECT_KEY = entry.getValue();
				break;
				}
			}
			initialized = true;
		}
		
		if (!initialized) {
			throw new JiraInitException("JIRA settings initialization failed.");
		}
	}
	public static void init() throws JiraInitException {
		init(false);
	}
	
	public static OAuthAccessor getAccessor() throws JiraInitException {
		return getAccessor(true);
	}
	public static OAuthAccessor getAccessor(boolean addAccessToken) throws JiraInitException {
		init();
		if (accessor == null) {
	    	// Create consumer object for accessor. null fields are for callback and service provider information (token request URLs) - neither of which are necessary
	        OAuthConsumer consumer = new OAuthConsumer("oob", CONSUMER_KEY, null, null);
	        
	        consumer.setProperty(RSA_SHA1.PRIVATE_KEY, PRIVATE_KEY);
	        consumer.setProperty(OAuth.OAUTH_SIGNATURE_METHOD, OAuth.RSA_SHA1);	        
	        
	        accessor = new OAuthAccessor(consumer);
		}
		
		if (addAccessToken) {
			accessor.accessToken = ACCESS_TOKEN;
		} else {
			accessor.accessToken = null;
		}
		
		return accessor;
	}
	

	
	public static String createTicket(String category, String summary, String description, String email, String name) throws OAuthException, IOException, URISyntaxException, JiraInitException {
		init();
		//json data template
		/*
		var issue = {
					"fields":{
						"project":{
							"key":"SHIIPHD"
						},
						"summary": request_summary,
						"description": request_description,
						"customfield_10603": user_name,
						"customfield_10606": user_email,
						"issuetype":{
							"name": request_type
						}
					}
				};
		 */
		
		JsonObject fields = new JsonObject();
		fields.addProperty("summary", summary);
		fields.addProperty("description", description);
		fields.addProperty("customfield_10603", name);//name
		fields.addProperty("customfield_10606", email);//email

		JsonObject project = new JsonObject();
		project.addProperty("key", PROJECT_KEY);
		fields.add("project", project);
		
		JsonObject issuetype = new JsonObject();
		issuetype.addProperty("name", category);
		fields.add("issuetype", issuetype);
		
		JsonObject issue = new JsonObject();
		issue.add("fields", fields);
		
        OAuthAccessor accessor = getAccessor();
        OAuthClient client = new OAuthClient(new HttpClient4());
        
        String strIssue = issue.toString();
        String url = BASE_URL + "/rest/api/2/issue/";
        int contentLength = strIssue.length();
        
        // Read JSON string received from user
        InputStream jsonStream = new ByteArrayInputStream(strIssue.getBytes(StandardCharsets.UTF_8));
        
        // Create request body
        OAuthMessage requestMessage = accessor.newRequestMessage(OAuthMessage.POST, url, Collections.<Map.Entry<?,?>>emptySet(), jsonStream);
        
        // Set request headers
        List<Map.Entry<String, String>> headers = requestMessage.getHeaders();
        headers.add(new OAuth.Parameter(HttpMessage.CONTENT_TYPE, "application/json"));
        headers.add(new OAuth.Parameter(HttpMessage.CONTENT_LENGTH, String.valueOf(contentLength)));
        
        // Send request
        OAuthMessage jiraResponse = client.invoke(requestMessage, ParameterStyle.QUERY_STRING);
        
        return jiraResponse.readBodyAsString();
	}
	
	/**
	 * 
	 * @return
	 * @throws OAuthException
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws JiraInitException 
	 */
	public static Map<String, String> getRequestToken() throws OAuthException, IOException, URISyntaxException, JiraInitException {
        OAuthAccessor accessor = getAccessor(false);
        OAuthClient client = new OAuthClient(new HttpClient4());
        
        String url = BASE_URL + "/plugins/servlet/oauth/request-token";

        // Create request body
        OAuthMessage requestMessage = accessor.newRequestMessage(OAuthMessage.POST, url, Collections.<Map.Entry<?,?>>emptySet());
        // Send request
        OAuthMessage jiraResponse = client.invoke(requestMessage, ParameterStyle.QUERY_STRING);
        String str = jiraResponse.readBodyAsString();
        
        LinkedHashMap<String, String> map = new LinkedHashMap<>();
        map.putAll(Splitter.on('&').trimResults().withKeyValueSeparator("=").split(str));
        
        
        if (!map.containsKey(OAuth.OAUTH_TOKEN)) {
        	throw new OAuthException("Didn't get oauth token. Response body: " + str);
        } else {
            map.put("authorize_url", BASE_URL + "/plugins/servlet/oauth/authorize?oauth_token=" + map.get(OAuth.OAUTH_TOKEN));
            map.put("response_body", str);
        }
        return map;
	}
	
	/**
	 *  Gets new request token from JIRA and saves it
	 * @param token
	 * @param secret
	 * @throws OAuthException
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws JiraInitException 
	 * @throws SQLException 
	 */
	public static boolean refreshAccessToken(String token, String secret) throws OAuthException, IOException, URISyntaxException, JiraInitException, SQLException {
	    OAuthAccessor accessor = getAccessor(false);
        accessor.requestToken = token;
        accessor.tokenSecret = secret;
        
        OAuthClient client = new OAuthClient(new HttpClient4());
        String url = BASE_URL + "/plugins/servlet/oauth/access-token";
        Collection<? extends Map.Entry<?, ?>> parameters = OAuth.newList(OAuth.OAUTH_TOKEN, token);
        
        OAuthMessage jiraResponse = client.invoke(accessor, OAuthMessage.POST,
                url, parameters);

        String str = jiraResponse.readBodyAsString();
        
        final Map<String, String> map = Splitter.on('&').trimResults().withKeyValueSeparator("=").split(str);
        if (!map.containsKey(OAuth.OAUTH_TOKEN)) {
        	throw new OAuthException("Didn't get oauth token. Response body: " + str);
        }
        //save the token to db
        String accessToken = map.get(OAuth.OAUTH_TOKEN);

		if (AcesSetting.updateSetting("JIRA_ACCESS_TOKEN", accessToken)) {
			//reload settings from db and also reset accessor
			init(true);
			return true;
		}
		return false;
	}
}
