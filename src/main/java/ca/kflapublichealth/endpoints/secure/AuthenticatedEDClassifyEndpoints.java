package ca.kflapublichealth.endpoints.secure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import ca.kflapublichealth.endpoints.EndpointHelper;
import ca.kflapublichealth.requests.RequestAuthenticator;
import ca.kflapublichealth.requests.RequestManager;

@Path("/secure/classify")
/**
 * Provides the remote service for accessing and retrieving Classified EDVisits data whilst
 * specifying a combination of parameters to make the data as customisable and specific as possible.
 * @author wzaghal
 *
 */
public class AuthenticatedEDClassifyEndpoints {
	static final Logger logger = LogManager.getLogger(AuthenticatedEDClassifyEndpoints.class);
	
	/**
	 * Returns a sum of total classified visits per day based on the different parameters provided.
	 * The parameters allow for a wide-variet of specific requests that are patient-specific,
	 * health unit specific, or classification-specific.
	 * 
	 * @param phuLocality Whether the health-unit 
	 * 
	 * @return
	 */
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/{locality}/phus/{PHU}")
	public static String getPhuClassVisits(@PathParam("locality") String phuLocality,
			@PathParam("PHU") String phu,
			@DefaultValue("json") @QueryParam("format") String format,
			@DefaultValue("All") @QueryParam("gender") String gender,
			@DefaultValue("0") @QueryParam("agefrom") int ageFrom,
			@DefaultValue("200") @QueryParam("ageto") int ageTo,
			@DefaultValue("0") @QueryParam("datefrom") long dateFrom,
			@DefaultValue("0") @QueryParam("dateto") long dateTo,
			@DefaultValue("YYYYYY") @QueryParam("ctas") String ctas,
			@DefaultValue("1") @QueryParam("page") int page,
			@DefaultValue("30") @QueryParam("pagesize") int pageSize,
			@DefaultValue("ME") @QueryParam("classifier") String classifier,
			@DefaultValue("S2014") @QueryParam("classification") String classification,
			@DefaultValue("All") @QueryParam("bucket") String bucket,
			@DefaultValue("All")@QueryParam("class") String classID,
			@QueryParam("token") String token,
			@DefaultValue("0") @QueryParam("probability") Double probability,
			@DefaultValue("All")@QueryParam("patient") String patientLocality,
			@DefaultValue("All")@QueryParam("filterByFSA") String fsa,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request,
			@DefaultValue("All")@QueryParam("hospital") String hospital,
			@QueryParam("type") String type,
			@DefaultValue("D")@QueryParam("grouping") String grouping,
			@DefaultValue("7")@QueryParam("weekStart") int weekStart,
			@DefaultValue("All")@QueryParam("fltrAmType") String fltrAmType) {
	
		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}
		
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("phu", phu);
		EndpointHelper.addAllParameter("fsa", fsa, params);
		EndpointHelper.addAllParameter("gender", gender, params);
		params.put("ageFrom", ageFrom);
		params.put("ageoTo", ageTo);
		EndpointHelper.addTheDateParameters(dateFrom, dateTo, params);
		EndpointHelper.addTheCtasParams(ctas, params);
		EndpointHelper.addTheLocalityParameters( phuLocality, patientLocality, params);
		params.put("classifier",classifier);
		params.put("classification",classification);
		
		EndpointHelper.addAllParameter("classID", classID, params);
		params.put("probability", probability);
		EndpointHelper.addAllParameter("bucket", bucket, params);
		params.put("hospital", hospital);

		params.put("token", token);
		
		params.put("grouping", grouping);
		params.put("weekStart", weekStart);
		params.put("fltrAmType", fltrAmType);
		
		HashMap<String, String> response;
		if (type.equals("AD")) {
			response = RequestAuthenticator
					.authenticateRequest(format, params, request, token,"getPhuClassAdmissions");
		} else {//default to ED
			response = RequestAuthenticator
					.authenticateRequest(format, params, request, token,"getPhuClassVisits");
		}
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}
		
		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}

	
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/ON")
	public static String getOnClassVisits(
			@DefaultValue("ed") @QueryParam("visitType") String visitType,
			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("token") String token,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {
	
		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}

		// TODO  Error checking pagesize and page
		
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("prov","ON");
		params.put("token", token);
		params.put("visitType", visitType);
		
		HashMap<String, String> response = RequestAuthenticator
				.authenticateRequest(format, params, request, token,"getONClassVisits");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}
		
		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}

}
