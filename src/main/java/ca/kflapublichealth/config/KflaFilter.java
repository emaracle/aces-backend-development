package ca.kflapublichealth.config;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
public class KflaFilter implements Filter{
	static final Logger logger = LogManager.getLogger(KflaFilter.class);

	public void doFilter(ServletRequest request,
						ServletResponse response,
						FilterChain chain) throws IOException, ServletException {
		try {
			if (request instanceof HttpServletRequest) {
				HttpServletRequest req = (HttpServletRequest) request;
				if (!isNormalized(req.getServletPath()) || !isNormalized(req.getPathInfo())) {
					throw new Exception("Un-normalized paths are not supported: "
							+ req.getServletPath()
							+ (req.getPathInfo() != null ? req.getPathInfo() : ""));
				}
			}
		}
		catch(Exception e) {
			logger.error(e.getMessage());
			throw new ServletException(e.getMessage());
		}

		// Call the next filter (continue request processing)
		chain.doFilter(request, response);
	}

	public void init(FilterConfig filterConfig) throws ServletException {}

	public void destroy() {}
	
	
	
	/**
	 * The method below is taken from https://github.com/spring-projects/spring-security/blob/master/web/src/main/java/org/springframework/security/web/firewall/DefaultHttpFirewall.java
	 * 
	 * Checks whether a path is normalized (doesn't contain path traversal sequences like
	 * "./", "/../" or "/.")
	 *
	 * @param path the path to test
	 * @return true if the path doesn't contain any path-traversal character sequences.
	 */
	private boolean isNormalized(String path) {
		if (path == null) {
			return true;
		}

		for (int j = path.length(); j > 0;) {
			int i = path.lastIndexOf('/', j - 1);
			int gap = j - i;

			if (gap == 2 && path.charAt(i + 1) == '.') {
				// ".", "/./" or "/."
				return false;
			}
			else if (gap == 3 && path.charAt(i + 1) == '.' && path.charAt(i + 2) == '.') {
				return false;
			}

			j = i;
		}

		return true;
	}
}