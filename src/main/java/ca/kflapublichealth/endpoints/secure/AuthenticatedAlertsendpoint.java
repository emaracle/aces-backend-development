package ca.kflapublichealth.endpoints.secure;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ca.kflapublichealth.endpoints.EndpointHelper;
import ca.kflapublichealth.requests.RequestAuthenticator;
import ca.kflapublichealth.requests.RequestManager;
@Path("/secure/alerts/")
/**
 *
 * @author wzaghal
 *
 */
public class AuthenticatedAlertsendpoint {
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/phus/{phu}")
	public static String getAlerts(
			@PathParam("phu") String phu,
			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("token") String token,
			@DefaultValue("0") @QueryParam("datefrom") long dateFrom,
			@DefaultValue("0") @QueryParam("dateto") long dateTo,
			//AlertTypeID comma seperated
			@DefaultValue("") @QueryParam("alertTypes") String alertTypes,
			//comma seperated ClassIDs
			@DefaultValue("") @QueryParam("syndromes") String syndromes,
			@DefaultValue("") @QueryParam("hospital") String hospital,
			@DefaultValue("") @QueryParam("geoType") String geoType,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {

		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}

		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("phu", phu);
		EndpointHelper.addTheDateParameters(dateFrom, dateTo, params);
		params.put("token", token);
		params.put("alertTypes", alertTypes);
		params.put("syndromes", syndromes);
		params.put("hospital", hospital);
		params.put("geoType", geoType);


		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "getAlerts");

		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		httpResponse.addHeader("Link", response.get("Header"));

		return response.get("Data");
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/alertid/{AlertID}")
	public static String getAlertPacket(
			@PathParam("AlertID") String AlertID,
			@DefaultValue("xml") @QueryParam("format") String format,
			@QueryParam("token") String token,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {

		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}

		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		params.put("AlertID", AlertID);

		HashMap<String, String> response =  RequestManager.getResponse(format,params, request, 0, "getAlertPacket");

		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		httpResponse.addHeader("Link", response.get("Header"));

		return response.get("Data");
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/epi/phus/{PHU}")
	public static String getPhuClassVisits(@PathParam("PHU") String phu,
			@DefaultValue("json") @QueryParam("format") String format,
			@DefaultValue("All") @QueryParam("gender") String gender,
			@DefaultValue("0") @QueryParam("agefrom") int ageFrom,
			@DefaultValue("200") @QueryParam("ageto") int ageTo,
			@DefaultValue("0") @QueryParam("datefrom") long dateFrom,
			@DefaultValue("0") @QueryParam("dateto") long dateTo,
			@DefaultValue("YYYYYY") @QueryParam("ctas") String ctas,
			@DefaultValue("ME") @QueryParam("classifier") String classifier,
			@DefaultValue("S2014") @QueryParam("classification") String classification,
			@DefaultValue("All") @QueryParam("bucket") String bucket,
			@DefaultValue("All")@QueryParam("class") String classID,
			@QueryParam("token") String token,
			@DefaultValue("All")@QueryParam("filterByFSA") String fsa,
			@DefaultValue("All")@QueryParam("hospital") String hospital,
			@DefaultValue("All") @QueryParam("locality") String phuLocality,
			@DefaultValue("All") @QueryParam("patient") String patientLocality,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {

		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}

		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("phu", phu);
		EndpointHelper.addAllParameter("fsa", fsa, params);
		EndpointHelper.addAllParameter("gender", gender, params);
		params.put("ageFrom", ageFrom);
		params.put("ageoTo", ageTo);
		EndpointHelper.addTheDateParameters(dateFrom, dateTo, params);
		EndpointHelper.addTheCtasParams(ctas, params);
		EndpointHelper.addTheLocalityParameters( phuLocality, patientLocality, params);
		params.put("classifier",classifier);
		params.put("classification",classification);

		EndpointHelper.addAllParameter("classID", classID, params);
		EndpointHelper.addAllParameter("bucket", bucket, params);
		params.put("hospital", hospital);
		params.put("token", token);


		HashMap<String, String> response = RequestAuthenticator.authenticateRequest(format, params, request, token,"getEpiAlerts");

		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}

		return response.get("Data");
	}
}
