package ca.kflapublichealth.endpoints;

import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ca.kflapublichealth.requests.RequestManager;

@Path("/AsthmaMapper")
public class AsthmaEndpoints {

	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/getPercentages")
	public static String getPercentages(
			@DefaultValue("json") @QueryParam("format") String format,
			@DefaultValue("") @QueryParam("LHIN") String LHIN,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {

		LinkedHashMap<String, Object> sqlParams = new LinkedHashMap<String, Object>();
		sqlParams.put("LHIN", LHIN);

		HashMap<String, String> response = RequestManager.getResponse(format, sqlParams, request, 0, "getAsthmaPercentage");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}
	
}