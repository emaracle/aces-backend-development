package ca.kflapublichealth.endpoints;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ca.kflapublichealth.requests.RequestManager;

@Path("/maps")
/**
 * Class that handles mapping-related user requests and
 * returns data in XML or JSON format.
 * @author wzaghal
 *
 */
public class ClassifiedMappingEndpoints {

	@Path("/choropleth")
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	/**
	 * Handles a user request to get the total visits or 'counts' for the requested geography level.
	 * The different geography levels include FSA, CD, CSD, PHU or LHIN. The user may also specify a number of other conditions
	 * such as a specific age group, gender as well as the classifier algorithm and classification schema to be used.
	 * There are a number of 'default' conditions set so that the user can not have to worry about all the conditions if he/she pleases.
	 * For more information on the default condition, look at the parameter list below.
	 * 
	 * Sample endpoints:
	 * 
	 * 
	 *  https://www._____:8080/____/aces/maps/choropleth?datefrom=1388588459000&dateto=1391266859000&type=FSA&class=RESP
	 *  -->  Returns the total FSA counts with data from January 1st 2014 to February 1st 2014. 
	 * 
	 * https://www._____:8080/____/aces/maps/choropleth?gender=f&datefrom=1391266859000&dateto=1396364459000&type=CSD
	 *  -->  Returns the total CSD counts with data from March 1st 2014 to April 1st 2014. 
	 * 
	 *
	 * 
	 * @param format
	 * @param gender
	 * @param ageFrom
	 * @param ageTo
	 * @param dateFrom
	 * @param dateTo
	 * @param ctas
	 * @param httpResponse
	 * @param request
	 * @param classifier
	 * @param classification
	 * @param classID
	 * @param probability
	 * @param type
	 * @return
	 */
	public static String getMappingCounts(

			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("gender") String gender,
			@DefaultValue("0") @QueryParam("agefrom") int ageFrom,
			@DefaultValue("200") @QueryParam("ageto") int ageTo,
			@DefaultValue("0") @QueryParam("datefrom") long dateFrom,
			@DefaultValue("0") @QueryParam("dateto") long dateTo,
			@DefaultValue("YYYYYY") @QueryParam("ctas") String ctas,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request,
			@DefaultValue("ME" ) @QueryParam("classifier") String classifier,
			@DefaultValue("S2014") @QueryParam("classification") String classification,
			@DefaultValue("All") @QueryParam("bucket") String bucket,
			@QueryParam("class") String classID,
			@DefaultValue("0") @QueryParam("probability") Double probability,
			@DefaultValue("FSA") @QueryParam("type") String type,
			@DefaultValue("1") @QueryParam("returnType") int returnType) {
		ArrayList<Object> sqlParams = new ArrayList<Object>();
		sqlParams.add(gender);
		sqlParams.add(ageFrom);
		sqlParams.add(ageTo);

		if (dateFrom != 0)
			sqlParams.add(dateFrom);
		else
			sqlParams.add(null);
		if (dateTo != 0)
			sqlParams.add(dateTo);
		else
			sqlParams.add(null);

		if (ctas.length() != 6) {
			// TODO error.
		}
		for (int i = 0; i < ctas.length(); i++) {
			Boolean result = false;
			if (ctas.charAt(i) == 'Y' || ctas.charAt(i) == 'y') {
				result = true;
			}
			sqlParams.add(result);
		}

		sqlParams.add(classifier);
		sqlParams.add(classification);
		if (classID == null) {
			sqlParams.add(null);
		} else
			sqlParams.add(classID);

		sqlParams.add(probability);
		sqlParams.add(type);
		sqlParams.add(bucket);
		sqlParams.add(returnType);

		HashMap<String, String> response = RequestManager.getResponse(format,
				sqlParams, request, 1, "getMappingCounts"); // <--- TODO - do I
															// even need this?
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}
		
		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}
	
}