package ca.kflapublichealth.output;

public class Syndrome {
	public String syndrome = null;
	public double probability = 0;
	
	public Syndrome(String syndrome, double probability) {
		this.syndrome = syndrome;
		this.probability = probability;
	}
}