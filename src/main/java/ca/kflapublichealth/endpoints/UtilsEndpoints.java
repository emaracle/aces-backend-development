package ca.kflapublichealth.endpoints;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;

@Path("/utils")
public class UtilsEndpoints {

	@POST
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/svgtoimage")
	public static String postSvgToImage(
			@FormParam("svg") String svg,
			@DefaultValue("chart") @FormParam("name") String name,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) throws IOException {
			
			//@todo authentication
		
		
			InputStream stream = new ByteArrayInputStream(svg.getBytes(StandardCharsets.UTF_8));         
		
			//we have obtained the SVG Image to InputStream. We can now convert it into JPEG.
			// Read input to TranscoderInput
			TranscoderInput input = new TranscoderInput(stream);    
			//Define output
			TranscoderOutput output;
			output = new TranscoderOutput(httpResponse.getOutputStream());
			//Create a PNG Transcoder
			PNGTranscoder transcoder = new PNGTranscoder();
			//do the conversion
			//httpResponse.setContentType("image/png");
			//return the output JPG image
			httpResponse.addHeader("Content-Disposition", "attachment; filename=".concat(name).concat(".png"));
			try {
				transcoder.transcode(input, output);
			} catch (TranscoderException e) {
				httpResponse.setContentType(MediaType.TEXT_HTML);
				httpResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);
				//System.out.println(e.getMessage());
				return null;
			}
			return null;
	}
	
}