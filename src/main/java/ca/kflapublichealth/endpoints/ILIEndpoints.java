package ca.kflapublichealth.endpoints;

import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ca.kflapublichealth.requests.RequestManager;

@Path("/ILIMapper")
public class ILIEndpoints {

	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/getPercentages")
	public static String getPercentages(
			@DefaultValue("json") @QueryParam("format") String format,
			@DefaultValue("") @QueryParam("PHU") String PHU,
			@DefaultValue("365") @QueryParam("numDays") Integer numDays,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {

		LinkedHashMap<String, Object> sqlParams = new LinkedHashMap<String, Object>();
		sqlParams.put("PHU", PHU);
		sqlParams.put("numDays", numDays);

		HashMap<String, String> response = RequestManager.getResponse(format, sqlParams, request, 0, "getILIPercentage");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}
 
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/getILI_Resp_Totals")
	public static String getILI_Resp_Totals(
			@DefaultValue("json") @QueryParam("format") String format, 
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {

		LinkedHashMap<String, Object> sqlParams = new LinkedHashMap<String, Object>(); 

		HashMap<String, String> response = RequestManager.getResponse(format, sqlParams, request, 0, "getILI_Resp_Totals");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	} 

	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/getFlu_totals")
	public static String getFlu_totals(
			@DefaultValue("json") @QueryParam("format") String format, 
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {

		LinkedHashMap<String, Object> sqlParams = new LinkedHashMap<String, Object>(); 

		HashMap<String, String> response = RequestManager.getResponse(format, sqlParams, request, 0, "getFlu_totals");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	} 
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/getILIMapper_Ontario_Colors")
	public static String getILI_MapperData_Ontario_Colors(
			@DefaultValue("json") @QueryParam("format") String format, 
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {
	
		LinkedHashMap<String, Object> sqlParams = new LinkedHashMap<String, Object>(); 
	
		HashMap<String, String> response = RequestManager.getResponse(format, sqlParams, request, 0, "getILIMapper_Ontario_Colors");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}
	
		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/getColours")
	public static String getColours(
			@DefaultValue("json") @QueryParam("format") String format,
			@DefaultValue("") @QueryParam("PHU") String PHU,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {

		LinkedHashMap<String, Object> sqlParams = new LinkedHashMap<String, Object>();
		sqlParams.put("PHU", PHU);

		HashMap<String, String> response = RequestManager.getResponse(format, sqlParams, request, 0, "getILIColours");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		if (format.equals("xml")) {
			httpResponse.setContentType("application/xml");
		}
		
		return response.get("Data");
	}
	
}