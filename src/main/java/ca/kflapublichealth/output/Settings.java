package ca.kflapublichealth.output;

public class Settings {
	
	public String version;
	public AppSettings appSettings;
	
	public Settings(String version, AppSettings appSettings) {
		this.version = version;
		this.appSettings = appSettings;
	}
}