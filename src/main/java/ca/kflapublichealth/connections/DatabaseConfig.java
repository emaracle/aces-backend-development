package ca.kflapublichealth.connections;

import java.io.InputStream;
import java.util.ArrayList;

import javax.servlet.ServletContext;

import com.thoughtworks.xstream.XStream;

import ca.kflapublichealth.requests.Request;
import ca.kflapublichealth.requests.RequestConverter;

public class DatabaseConfig {

	static DatabaseModel databaseModel;
	static final String path = "/Properties/DatabaseConfig.xml";
	
	public static void populateDatabaseConfig(ServletContext context){ 
		System.out.println("Using database props file: "+path);
		InputStream is = context.getResourceAsStream(path);
		XStream xstream = new XStream(); 
		databaseModel = (DatabaseModel) xstream.fromXML(is);
		System.out.println("DatabaseModel set for later use"); 
	}
	
	public DatabaseModel getDatabaseModel() {
		return databaseModel;
	}
}
