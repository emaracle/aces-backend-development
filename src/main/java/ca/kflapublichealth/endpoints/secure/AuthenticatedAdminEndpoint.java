package ca.kflapublichealth.endpoints.secure;
import java.util.HashMap;
import java.util.LinkedHashMap;

import java.io.IOException;
import java.net.URISyntaxException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ca.kflapublichealth.requests.RequestManager;
import net.oauth.OAuthException;

@Path("/secure/admin/")
public class AuthenticatedAdminEndpoint {

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/users")
	public static String getUsers(
			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("token") String token,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) throws OAuthException, IOException, URISyntaxException {

		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}

		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);

		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "getUsers");

		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		return response.get("Data");
	}

	/**
	 * Used to check if unique username
	 * @param username
	 * @param format
	 * @param token
	 * @param httpResponse
	 * @param request
	 * @return
	 * @throws OAuthException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/user/useruuid-by-username/{username}")
	public static String getUserUUIDByUsername(
			@PathParam("username") String username,
			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("token") String token,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) throws OAuthException, IOException, URISyntaxException {

		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}

		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		params.put("username", username);

		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "getUserUUIDByUsername");

		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		return response.get("Data");
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/user")
	public static String createUser(
			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("token") String token,
			@FormParam("UserName") String UserName,
			@FormParam("Password") String Password,
			@FormParam("FName") String FName,
			@FormParam("LName") String LName,
			@FormParam("Email") String Email,
			@FormParam("Telephone") String Telephone,
			@FormParam("Agency") String Agency,
			@FormParam("Position") String Position,
			@FormParam("isActive") boolean isActive,
			@FormParam("PermissionType") String PermissionType,
			@FormParam("PermissionValue") String PermissionValue,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) throws OAuthException, IOException, URISyntaxException {

		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		params.put("UserName", UserName);
		params.put("Password", Password);
		params.put("FName", FName);
		params.put("LName", LName);
		params.put("Email", Email);
		params.put("Telephone", Telephone);
		params.put("Agency", Agency);
		params.put("Position", Position);
		params.put("isActive", isActive);
		params.put("PermissionType", PermissionType);
		params.put("PermissionValue", PermissionValue);

		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "createUser");

		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		return response.get("Data");
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/user/{UserUUID}/getAppSettings")
	public static String getAppSettings(
			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("token") String token,
			@PathParam("UserUUID") String UserUUID,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) throws OAuthException, IOException, URISyntaxException {

		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}

		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		params.put("UserUUID", UserUUID);

		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "getAppSettings");

		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		return response.get("Data");
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/user/{UserUUID}/updateAppSetting")
	public static String updateAppSetting(
			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("token") String token,
			@PathParam("UserUUID") String UserUUID,
			@FormParam("PermissionUUID") String PermissionUUID,
			@FormParam("PermissionValue") String PermissionValue,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) throws OAuthException, IOException, URISyntaxException {

		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}

		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		params.put("UserUUID", UserUUID);
		params.put("PermissionUUID", PermissionUUID);
		params.put("PermissionValue", PermissionValue);

		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "updateAppSetting");

		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		return response.get("Data");
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/user/{UserUUID}/updateUserDetails")
	public static String updateUserDetails(
			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("token") String token,
			@PathParam("UserUUID") String UserUUID,
			@FormParam("FName") String FName,
			@FormParam("LName") String LName,
			@FormParam("Email") String Email,
			@FormParam("Telephone") String Telephone,
			@FormParam("Agency") String Agency,
			@FormParam("Position") String Position,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) throws OAuthException, IOException, URISyntaxException {

		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		params.put("UserUUID", UserUUID);
		params.put("FName", FName);
		params.put("LName", LName);
		params.put("Email", Email);
		params.put("Agency", Agency);
		params.put("Position", Position);
		params.put("Telephone", Telephone);

		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "updateUserDetails");

		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		return response.get("Data");
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/user/{UserUUID}/setPasswordAdmin")
	public static String setPasswordAdmin(
			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("token") String token,
			@PathParam("UserUUID") String UserUUID,
			@FormParam("pswd") String pswd,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) throws OAuthException, IOException, URISyntaxException {

		//Get header token
		String headerToken = request.getHeader("token");
		if(headerToken != null && headerToken.length() > 0){
   			token = headerToken;
   		}

		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		params.put("UserUUID", UserUUID);
		params.put("pswd", pswd);

		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "setPasswordAdmin");

		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		return response.get("Data");
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/user/{UserUUID}/status")
	public static String updateUserStatus(
			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("token") String token,
			@PathParam("UserUUID") String UserUUID,
			@FormParam("isActive") boolean isActive,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) throws OAuthException, IOException, URISyntaxException {

		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		params.put("UserUUID", UserUUID);
		params.put("isActive", isActive);

		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "updateUserStatus");

		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		return response.get("Data");
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/user/{UserUUID}/archived")
	public static String updateUserArchived(
			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("token") String token,
			@PathParam("UserUUID") String UserUUID,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) throws OAuthException, IOException, URISyntaxException {

		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}

		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		params.put("UserUUID", UserUUID);

		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "updateUserArchived");

		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		return response.get("Data");
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/user/{UserUUID}/resources")
	public static String getUserResources(
			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("token") String token,
			@PathParam("UserUUID") String UserUUID,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) throws OAuthException, IOException, URISyntaxException {

		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		params.put("UserUUID", UserUUID);

		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "getUserResources");

		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		return response.get("Data");
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/user/{UserUUID}/resources/{ID}")
	public static String updateUserResource(
			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("token") String token,
			@PathParam("UserUUID") String UserUUID,
			@PathParam("ID") Integer ID,
			@FormParam("PermissionType") String PermissionType,
			@FormParam("PermissionValue") String PermissionValue,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) throws OAuthException, IOException, URISyntaxException {

		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		params.put("ID", ID);
		params.put("UserUUID", UserUUID);
		params.put("PermissionType", PermissionType);
		params.put("PermissionValue", PermissionValue);

		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "updateUserResource");

		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		return response.get("Data");
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/user/{UserUUID}/resources")
	public static String addUserResource(
			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("token") String token,
			@PathParam("UserUUID") String UserUUID,
			@FormParam("PermissionType") String PermissionType,
			@FormParam("PermissionValue") String PermissionValue,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) throws OAuthException, IOException, URISyntaxException {

		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		params.put("UserUUID", UserUUID);
		params.put("PermissionType", PermissionType);
		params.put("PermissionValue", PermissionValue);

		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "addUserResource");

		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		return response.get("Data");
	}
	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/user/{UserUUID}/resources/{ID}/delete")
	public static String deleteUserResource(
			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("token") String token,
			@PathParam("UserUUID") String UserUUID,
			@PathParam("ID") Integer ID,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) throws OAuthException, IOException, URISyntaxException {

		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		params.put("ID", ID);
		params.put("UserUUID", UserUUID);

		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "deleteUserResource");

		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		return response.get("Data");
	}
}
