package ca.kflapublichealth.endpoints.secure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import ca.kflapublichealth.requests.RequestManager;

@Path("/secure/lookup")
/**
 * Provides the remote service to access and retrieve reference tables that can be used to understand
 * the data that the other endpoints provide
 * 
 * @author wzaghal
 */
public class AuthenticatedEDLookupEndpoints {
	
	/**
	 * Returns Hospital and PHU information
	 * @param format
	 * @param sortBy
	 *            Name, or PHUID.
	 * @param httpResponse
	 * @param request
	 * @return
	 */
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/phus-and-hosps")
	public static String getHospsAndPhus(
			@DefaultValue("json") @QueryParam("format") String format,
			@QueryParam("token") String token,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {
	
		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}
		
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("token", token);
		HashMap<String, String> response =  RequestManager.getAuthenticatedResponse(format,params,
				request,0, "getAuthHospsAndPhus");
		
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}
		
		httpResponse.addHeader("Link", response.get("Header"));

		return response.get("Data");
	}	

}
