/** Handles servlets, logging and other behind-the-scenes configuration.
 */
package ca.kflapublichealth.config;