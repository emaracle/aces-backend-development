package ca.kflapublichealth.config;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

/**
 * Provides Cross-origin sharing for client applications 
 * @author wzaghal
 *
 */

@Provider
public class CrossOriginResourceSharingFilter implements ContainerResponseFilter {
	
	/**
	 * Allows Cross-origin sharing by making changes to the header
	 */
	@Override
	public void filter(ContainerRequestContext creq, ContainerResponseContext cresp) throws IOException {

		cresp.getHeaders().add("Access-Control-Allow-Origin", "*");
		cresp.getHeaders().add("Access-Control-Allow-Credentials", "true");
		cresp.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
		cresp.getHeaders().add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, token");
		cresp.getHeaders().add("Access-Control-Expose-Headers", "token");

	}

}