package ca.kflapublichealth.config;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

//import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
/**
 * IMPLEMENTATION NOTE: Resolves an issue with FormParam processing
 * We do not need this filter for Glassfish 4.1
 * Based on http://stackoverflow.com/questions/17602432/jersey-and-formparam-not-working-when-charset-is-specified-in-the-content-type
 * @see https://java.net/jira/browse/JERSEY-1978 
 * @see https://java.net/jira/browse/JERSEY-1900
 * @author nlateng
 *
 */

@Provider
@PreMatching
public class SanitizeContentTypeHeaderFilter implements ContainerRequestFilter {
//	static final Logger logger = LogManager.getLogger(SanitizeContentTypeHeaderFilter.class);
	
	@Override
	public void filter(ContainerRequestContext creq) throws IOException {
		MultivaluedMap<String,String> headers = creq.getHeaders();
        List<String> contentTypes = headers.remove(HttpHeaders.CONTENT_TYPE);
        if (contentTypes != null && !contentTypes.isEmpty()) {
            String contentType = contentTypes.get(0);
            //logger.debug("unsanitizedContentType is " + contentType);
            //keep only the first part of the content-type string
            String sanitizedContentType = contentType.split(";")[0].trim();
            //logger.debug("sanitizedContentType is " + sanitizedContentType);
        	//headers.add(HttpHeaders.CONTENT_TYPE, contentType);
            headers.add(HttpHeaders.CONTENT_TYPE, sanitizedContentType);
        }
	}
}