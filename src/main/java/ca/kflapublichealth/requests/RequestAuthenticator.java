package ca.kflapublichealth.requests;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import ca.kflapublichealth.connections.DatabaseConnection;
import ca.kflapublichealth.endpoints.AuthenticateEndpoints;

/**
 * Class that authenticates user requests based on user permissions found in the
 * database
 * 
 * @author wzaghal
 * 
 */
public class RequestAuthenticator {

	static final Logger logger = LogManager.getLogger(RequestAuthenticator.class);
	static ResultSet redirectingResultSet;

	/**
	 * Connects to Database, gets permissions,checks permissions and depending
	 * on output, calls the database.
	 */
	private static ArrayList<Object> verifyPermissions(String useruuid,
			String type, String dataValue, LinkedHashMap<String, Object> Params) {

		RequestReference reqReference = new RequestReference();
		Request req = reqReference.getRequest("getAuthenticationPermissions");
		String storedProc = req.getStoredProcedure();
		ArrayList<Object> permissionParams = new ArrayList<Object>();
		permissionParams.add(useruuid);
		permissionParams.add(type);
		permissionParams.add(dataValue);

		ResultSet PermissionsResults = DatabaseConnection.getData(
				req.getStoredProcedure(), permissionParams);
		ArrayList<Object> PermissionsresultList = RequestProcessor
				.getDataFromResultSet(PermissionsResults);

		logger.debug("Verified Permissions");

		return PermissionsresultList;

	}

	/**
	 * Given a requestID, uuid and a list of parameters, check that the user has
	 * the right permissions
	 * 
	 * @param uuid
	 * @param requestID
	 * @param sqlParams
	 */
	private static int checkPermissions(ArrayList<Object> resultList) {

		for (Object mp : resultList) {
			Map<Object, Object> map = (HashMap<Object, Object>) mp;

			Iterator it = map.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pairs = (Map.Entry) it.next();
				Object key = pairs.getKey();
				Object value = pairs.getValue();
				if (key.toString().equals("SuccessMessage")) {
					return 0;
				}

				if (key.toString().equals("ErrorMessage")) {
					logger.debug(value);
					return -1;
				}

			}

		}
		return 1;

	}
	
	public static boolean validToken(String token) {
		// Verify Permissions
		ArrayList<Object> results = RequestAuthenticator.verifyPermissions(token, "", "", null);

		// Get response
		int permissionResponse = RequestAuthenticator.checkPermissions(results);

		logger.debug("response is : " + permissionResponse);

		if (permissionResponse==-1) {
			return false;
		} else {
			return true;
		}
	}
	
	private static String getPermissionDataType(LinkedHashMap<String, Object> params) {
		String dataType = "";

		if ((params.get("prov") != null)) {
			dataType = "prov";
		} else if ((params.get("phu") != null)) {
			dataType = "phu";
		} else if ((params.get("hosp") != null)) {
			dataType = "hosp";
		} else if ((params.get("fsa") != null)) {
			dataType = "fsa";
		}
		
		return dataType;
	}
	
	private static Object getPermissionDataValue(LinkedHashMap<String, Object> params, String type) {
		if (!(type.isEmpty()) && params.containsKey(type)) {
			return params.get(type);	
		} else {
			return "";
		}		
	}

	/**
	 * Checks that the user is authorized to submit the request submitted. This
	 * is done in 3 different parts:
	 * 
	 * 1. Understanding what type of data the request will return
	 * (province,hospital, health-unit or postal code)
	 * 
	 * 2. Checking the user's access level to access that data type
	 * 
	 * 3. If the user is authorized to access that data, then the RequestAuthenticator
	 * 
	 * 
	 * @param format
	 * @param params
	 * @param request
	 * @param userid
	 * @param requestID
	 * @return
	 */
	public static HashMap<String, String> authenticateRequest(String format, LinkedHashMap<String, Object> params, HttpServletRequest request, String userid, String requestID) {
		
		HashMap<String, String> response = new HashMap<String, String>();
		String finalResponse = "";
		String token = (String) params.get("token");
		if (RequestAuthenticator.validToken(token)) { // Success
			return RequestManager.getAuthenticatedResponse(format, params, request, 0, requestID);
			
		} else {
			RequestManager.auditLog(request, javax.servlet.http.HttpServletResponse.SC_FORBIDDEN, params, null);
			throw new WebApplicationException(javax.servlet.http.HttpServletResponse.SC_FORBIDDEN);

		}
	}

	public static String mergeSubRequests(String finalResponse, String format,
			LinkedHashMap<String, Object> sqlParams,
			HttpServletRequest request, int page, String requestID) {

		// Merge previous json data with new json data
		if (finalResponse.length() > 1) {
			String incomingSubRequest = RequestManager
					.getAuthenticatedResponse(format, sqlParams, request, page,
							requestID).get("Data");
			incomingSubRequest = incomingSubRequest.replaceAll("\\[", "");
			finalResponse = finalResponse.replaceAll("\\]", "");
			finalResponse += "," + incomingSubRequest;

		} else {
			finalResponse = RequestManager.getAuthenticatedResponse(format,
					sqlParams, request, page, requestID).get("Data");
		}

		return finalResponse;

	}

}
