package ca.kflapublichealth.endpoints;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * A collection of classes that assist in preparing and collecting all the
 * parameters that are to be passed to the database level
 * 
 * @author wzaghal
 * 
 */
public class EndpointHelper {

	/**
	 * Adds the many different CTAS parameters to sqlParams based on the ctas
	 * parameter.
	 * 
	 * @param ctas
	 *            a String consisting of 6 'Y' or 'N' characters, with each
	 *            character representing its respective ctas level
	 * @param sqlParams
	 *            a linkedhashmap containing all the collected parameters from
	 *            the user's endpoint request.
	 * @return An updated linked hash map (sqlParams) containing the many CTAS
	 *         parameters
	 */
	public static LinkedHashMap<String, Object> addTheCtasParams(String ctas,
			LinkedHashMap<String, Object> sqlParams) {

		if (ctas.length() != 6) {
			// TODO - Error
		}
		for (int i = 0; i < ctas.length(); i++) {
			Boolean ctasResult = false;
			if (ctas.charAt(i) == 'Y' || ctas.charAt(i) == 'y') {
				ctasResult = true;
			}
			sqlParams.put("ctas" + (i + 1), ctasResult);
		}
		return sqlParams;
	}

	/**
	 * Adds the date parameters to sqlParams based on the dateto and datefrom
	 * parameters
	 * 
	 * @param dateTo
	 *            the starting date
	 * @param dateFrom
	 *            the ending date
	 * @param sqlParams
	 *            a linkedhashmap containing all the collected parameters from
	 *            the user's endpoint request.
	 * @return An updated linked hash map (sqlParams) containing the date
	 *         parameters
	 */
	public static LinkedHashMap<String, Object> addTheDateParameters(
			long dateFrom, long dateTo, LinkedHashMap<String, Object> sqlParams) {
		if (dateFrom != 0)
			sqlParams.put("datefrom", dateFrom);
		else
			sqlParams.put("datefrom", null);
		if (dateTo != 0)
			sqlParams.put("dateto", dateTo);
		else
			sqlParams.put("dateto", null);

		return sqlParams;

	}

	/**
	 * Adds parameters that describe locality and correctly to the parameters
	 * list.
	 * 
	 * @param phuLocality
	 *            String describing health unit as local or non-local
	 * @param patientLocality
	 *            String describing hospital as local or non-local
	 * @param sqlParams
	 *            List of parameters that will be passed to retrieve data
	 * @return An updated linked hash map (sqlParams) containing the locality
	 *         parameters
	 */
	public static LinkedHashMap<String, Object> addTheLocalityParameters(
			String phuLocality, String patientLocality,
			LinkedHashMap<String, Object> sqlParams) {

		if (phuLocality.equals("local")) {
			sqlParams.put("phuIsLocal", true);
			sqlParams.put("phuIsNotLocal", false);
		} else if (phuLocality.equals("non-local")) {
			sqlParams.put("phuIsLocal", false);
			sqlParams.put("phuIsNotLocal", true);

		} else if (phuLocality.equals("All")) {
			sqlParams.put("phuIsLocal", true);
			sqlParams.put("phuIsNotLocal", true);
		} else { // TODO - Error
			sqlParams.put("locality", false);
		}
		if (patientLocality.equals("local")) {
			sqlParams.put("patientIsLocal", true);
			sqlParams.put("patientIsNotLocal", false);
		} else if (patientLocality.equals("non-local")) {
			sqlParams.put("patientIsLocal", false);
			sqlParams.put("patientIsNotLocal", true);
		} else if (patientLocality.equals("All")) {
			sqlParams.put("patientIsLocal", true);
			sqlParams.put("patientIsNotLocal", true);
		} else { // TODO - Error
			sqlParams.put("patientIsLocal", false);
			sqlParams.put("patientIsNotLocal", true);
		}
		return sqlParams;
	}

	/**
	 * Add parameters that may have for an all-inclusive 'all' value to the
	 * parameters list
	 * 
	 * @param Key
	 *            the title of the parameter
	 * @param parameter
	 *            the value of the parameter
	 * @param sqlParams
	 *            List of parameters that will be passed to retrieve data
	 * @return An updated linked hash map (sqlParams) containing the locality
	 *         parameters
	 */
	public static LinkedHashMap<String, Object> addAllParameter(String Key,
			String parameter, LinkedHashMap<String, Object> sqlParams) {
		//if (parameter.toUpperCase().equals("ALL")) {
			// add as null so SQL Query can ignore variable and include all
			// cases
		//	sqlParams.put(Key, null);
		//} else {
			sqlParams.put(Key, parameter);
		//}

		return sqlParams;
	}

}
