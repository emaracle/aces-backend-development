package ca.kflapublichealth.endpoints;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Date;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.lang.reflect.Type;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.Produces;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import ca.kflapublichealth.requests.RequestManager;
import ca.kflapublichealth.output.OutputFormat;
import ca.kflapublichealth.output.UserData;
import ca.kflapublichealth.output.Settings;

/**
 * Class that handles user-authentication handshaking process.
 * This involves logging in, retrieving the users session token and logging out.
 *
 * @author avarrette
 */

/**
 *
 * @author wzaghal
 *
 */
@Path("/authenticate")
public class AuthenticateEndpoints {
	static final Logger logger = LogManager.getLogger(AuthenticateEndpoints.class);

	/**
	 *
	 * @param userid the users unique id
	 * @param format the format of the data returned. XML or JSON
	 * @param pswd the users password
	 * @param httpResponse internal parameter
	 * @param request internal parameter
	 * @return
	 */

	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/login")
	public static String getUserID(@FormParam("userid") String userid,
			@DefaultValue("json") @QueryParam("format") String format,
			@FormParam("pswd") String pswd,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {


		LinkedHashMap<String, Object> sqlParams = new LinkedHashMap<String, Object>();
		sqlParams.put("userid", userid);
		sqlParams.put("pswd", pswd);
		sqlParams.put("ip", RequestManager.getRemoteAddr(request));

		HashMap<String, String> response = RequestManager.getResponse(format, sqlParams, request, 0, "getUserIDDB");
		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		return response.get("Data");
	}

	/**
	 *
	 * @param token
	 * @param format
	 * @param httpResponse
	 * @param request
	 * @return
	 */
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/logout")
	public static String logoutUser(@QueryParam("token") String token,
			@DefaultValue("json") @QueryParam("format") String format,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {

		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}

		LinkedHashMap<String, Object> sqlParams = new LinkedHashMap<String, Object>();
		sqlParams.put("token", token);

		HashMap<String, String> response = RequestManager.getResponse(format, sqlParams, request, 0, "logoutUserToken");

		return response.get("Data");
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/forgotUsername")
	public static String forgotUsername(@DefaultValue("json") @QueryParam("format") String format,
										@Context HttpServletResponse httpResponse,
										@Context HttpServletRequest request,
										@FormParam("email") String email) {

		LinkedHashMap<String, Object> sqlParams = new LinkedHashMap<String, Object>();
		sqlParams.put("email", email);

		return RequestManager.getResponse(format, sqlParams, request, 0, "forgotUsername").get("Data");
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/forgotPassword")
	public static String forgotPassword(@DefaultValue("json") @QueryParam("format") String format,
										@Context HttpServletResponse httpResponse,
										@Context HttpServletRequest request,
										@FormParam("email") String email,
										@FormParam("changePwdURL") String changePwdURL) {

		LinkedHashMap<String, Object> sqlParams = new LinkedHashMap<String, Object>();
		sqlParams.put("email", email);
		sqlParams.put("changePwdURL", changePwdURL);

		HashMap<String, String> response = RequestManager.getResponse(format, sqlParams, request, 0, "forgotPassword");

		return response.get("Data");
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/changePassword")
	public static String changePassword(@DefaultValue("json") @QueryParam("format") String format,
										@Context HttpServletResponse httpResponse,
										@Context HttpServletRequest request,
										@FormParam("userid") String userid,
										@FormParam("pswd") String pswd,
										@FormParam("token") String token,
										@DefaultValue("0") @FormParam("isreset") Boolean isReset) {

		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}

		//Don't check is password is the same if resetting
		if (isReset.equals("0")){
			//Evaluate Password to ensure new password is different from old password
			LinkedHashMap<String, Object> sqlCheckPasswordParams = new LinkedHashMap<String, Object>();
			sqlCheckPasswordParams.put("userid", userid);
			sqlCheckPasswordParams.put("token", token);
			sqlCheckPasswordParams.put("pswd", pswd);

			HashMap<String, String> checkPasswordResponse = RequestManager.getResponse(format, sqlCheckPasswordParams, request, 0, "checkPassword");
		}

		//Change password
		LinkedHashMap<String, Object> sqlParams = new LinkedHashMap<String, Object>();
		sqlParams.put("userid", userid);
		sqlParams.put("pswd", pswd);
		sqlParams.put("token", token);

		HashMap<String, String> response = RequestManager.getResponse(format, sqlParams, request, 0, "changePassword");

		//Get userUUID from userid
		LinkedHashMap<String, Object> sqlUserParams = new LinkedHashMap<String, Object>();
		sqlUserParams.put("userid", userid);
		sqlUserParams.put("pswd", pswd);
		sqlUserParams.put("ip", RequestManager.getRemoteAddr(request));

		HashMap<String, String> userResponse = RequestManager.getResponse(format, sqlUserParams, request, 0, "getUserIDDB");

		//Put user data in JSON backing object
		Type userListType = new TypeToken<List<UserData>>() {}.getType();
		List<UserData> userDataList = new Gson().fromJson(userResponse.get("Data"), userListType);

		//Get user settings
		LinkedHashMap<String, Object> sqlGetSettingsParams = new LinkedHashMap<String, Object>();
		sqlGetSettingsParams.put("UserUUID", userDataList.get(0).useruuid);
		sqlGetSettingsParams.put("token", token);

		HashMap<String, String> getSettingsResponse = RequestManager.getResponse("detect", sqlGetSettingsParams, request, 0, "getUserSettings");

		if (!getSettingsResponse.get("Data").toString().equals("[{}]")){
			//Sanitize JSON string
			String strSettings = getSettingsResponse.get("Data").toString().replace("\\", "").replace("\"{","{").replace("}\"}","}}");

			//Put user settings in JSON backing object
			JsonArray jsonArraySettings = (JsonArray)new JsonParser().parse(strSettings);
			Settings settingsObject = new Gson().fromJson(jsonArraySettings.get(0).getAsJsonObject().get("Settings") , Settings.class);

			//Update appSettings
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			settingsObject.appSettings.forcePasswordChange = false;
		   	settingsObject.appSettings.passwordLastUpdated = dateFormat.format(new Date());
			String xmlString = OutputFormat.jsonToXml(new Gson().toJson(settingsObject));

			//Set user settings
			LinkedHashMap<String, Object> sqlSetSettingsParams = new LinkedHashMap<String, Object>();
			sqlSetSettingsParams.put("UserUUID", userDataList.get(0).useruuid);
			sqlSetSettingsParams.put("settings", xmlString);
			sqlSetSettingsParams.put("token", token);

			HashMap<String, String> setSettingsResponse = RequestManager.getResponse(format, sqlSetSettingsParams, request, 0, "setUserSettings");
		}

		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		return response.get("Data");
	}

	/**
	 *
	 * @param token
	 * @param format
	 * @param httpResponse
	 * @param request
	 * @return the users token id
	 */
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/user")
	public static String getUserToken(@QueryParam("token") String token,
			@DefaultValue("json") @QueryParam("format") String format,
			@Context HttpServletResponse httpResponse,
			@Context HttpServletRequest request) {

		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}

		LinkedHashMap<String, Object> sqlParams = new LinkedHashMap<String, Object>();
		sqlParams.put("token", token);

		HashMap<String, String> response = RequestManager.getResponse(format, sqlParams, request, 0, "getUserToken");

		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		return response.get("Data");
	}


	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/getSettings/{userid}")
	public static String getSettings(@QueryParam("token") String token,
								  @DefaultValue("json") @QueryParam("format") String format,
								  @PathParam("userid") String userid,
								  @Context HttpServletResponse httpResponse,
								  @Context HttpServletRequest request) {

		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}

		LinkedHashMap<String, Object> sqlInitParams = new LinkedHashMap<String, Object>();
		sqlInitParams.put("UserUUID", userid);
		sqlInitParams.put("token", token);

		//Set format to detect, so the format can be determined in the requestManager once the data is retrieved from the db
		HashMap<String, String> initSettingResponse = RequestManager.getResponse("detect", sqlInitParams, request, 0, "getUserSettings");

		if (!initSettingResponse.get("Data").toString().equals("[{}]")){
			//Sanitize JSON string
			String strSettings = initSettingResponse.get("Data").toString().replace("\\", "").replace("\"{","{").replace("}\"}","}}");

			//Put user settings in JSON backing object
			JsonArray jsonArraySettings = (JsonArray)new JsonParser().parse(strSettings);
			Settings settingsObject = new Gson().fromJson(jsonArraySettings.get(0).getAsJsonObject().get("Settings") , Settings.class);

			//Evaluate date since last password change
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			if (settingsObject.appSettings.passwordLastUpdated != null ){
				try {
					Date dteCurrentDate = new Date();
					Date dtePasswordLastUpdated = dateFormat.parse(settingsObject.appSettings.passwordLastUpdated);
					Long lngDiffDays = (dteCurrentDate.getTime() - dtePasswordLastUpdated.getTime())/(24*60*60*1000);

					//If password is older than X number of days, set force change to true
					if (lngDiffDays >= 90){
						//Update appSettings
					   	settingsObject.appSettings.forcePasswordChange = true;
						String xmlString = OutputFormat.jsonToXml(new Gson().toJson(settingsObject));

						//Set user settings
						LinkedHashMap<String, Object> sqlSetSettingsParams = new LinkedHashMap<String, Object>();
						sqlSetSettingsParams.put("UserUUID", userid);
						sqlSetSettingsParams.put("settings", xmlString);
						sqlSetSettingsParams.put("token", token);

						HashMap<String, String> setSettingsResponse = RequestManager.getResponse("json", sqlSetSettingsParams, request, 0, "setUserSettings");
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			} else {
				//Update appSettings
			   	settingsObject.appSettings.passwordLastUpdated = dateFormat.format(new Date());
				String xmlString = OutputFormat.jsonToXml(new Gson().toJson(settingsObject));

				//Set user settings
				LinkedHashMap<String, Object> sqlSetSettingsParams = new LinkedHashMap<String, Object>();
				sqlSetSettingsParams.put("UserUUID", userid);
				sqlSetSettingsParams.put("settings", xmlString);
				sqlSetSettingsParams.put("token", token);

				HashMap<String, String> setSettingsResponse = RequestManager.getResponse("json", sqlSetSettingsParams, request, 0, "setUserSettings");
			}
		}

		//Get updated settings
		LinkedHashMap<String, Object> sqlParams = new LinkedHashMap<String, Object>();
		sqlParams.put("UserUUID", userid);
		sqlParams.put("token", token);

		//Set format to detect, so the format can be determined in the requestManager once the data is retrieved from the db
		HashMap<String, String> response = RequestManager.getResponse("detect", sqlParams, request, 0, "getUserSettings");

		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		return response.get("Data");
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_ATOM_XML })
	@Path("/setSettings/{userid}")
	public static String setSettings(@QueryParam("token") String token,
								  @DefaultValue("json") @QueryParam("format") String format,
								  @PathParam("userid") String userid,
								  @FormParam("value") String value,
								  @Context HttpServletResponse httpResponse,
								  @Context HttpServletRequest request) {

		java.util.Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName.equals("token") && !(request.getHeader("token").equals(""))) {
				token = request.getHeader("token");
				break;
			}
		}

		LinkedHashMap<String, Object> sqlParams = new LinkedHashMap<String, Object>();

		//Check is valid JSON
		Boolean isJsonValid = false;
		String userSettingsString = value;
		isJsonValid = OutputFormat.isJSONValid(userSettingsString);

		//If valid JSON, convert to XML
		if (isJsonValid){
			String xmlString = OutputFormat.jsonToXml(userSettingsString);
			value = xmlString;
		}

		//Set parameters (settings as XML)
		sqlParams.put("UserUUID", userid);
		sqlParams.put("settings", value);
		sqlParams.put("token", token);

		HashMap<String, String> response = RequestManager.getResponse(format, sqlParams, request, 0, "setUserSettings");

		if (response.containsKey("token")) {
			httpResponse.addHeader("token", response.get("token"));
		}

		return response.get("Data");

	}

}
