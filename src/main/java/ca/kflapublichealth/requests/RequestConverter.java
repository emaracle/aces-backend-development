package ca.kflapublichealth.requests;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class RequestConverter implements Converter {
	static final Logger logger = LogManager.getLogger(RequestConverter.class);
	@Override
	public boolean canConvert(Class theClass) {
		return theClass.equals(RequestReference.class);
	}
	

	@Override
	public void marshal(Object obj, HierarchicalStreamWriter writer,
			MarshallingContext arg2) {

	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader,
			UnmarshallingContext context) {
		boolean isComplete = false;
		RequestReference reqConfig = new RequestReference();
		
		ArrayList<Request> reqList = new ArrayList<Request>();
		reader.moveDown(); // move to Request
		Request newReq = new Request();
		Request childReq = new Request();
		
		while (!isComplete) {

			reader.moveDown();

			newReq.setName(reader.getValue());
			logger.debug("Name : " + reader.getValue());
			reader.moveUp();
			reader.moveDown();
			newReq.setDescription(reader.getValue());
			logger.debug("Desc : " + reader.getValue());
			reader.moveUp();
			reader.moveDown();
			newReq.setStoredProcedure(reader.getValue());
			logger.debug("Proc : " + reader.getValue());
			reader.moveUp();
			
			if (reader.hasMoreChildren()){
				reader.moveDown();
				reader.moveDown();
				childReq.setName(reader.getValue());
				logger.debug("Child Name : " + reader.getValue());
				reader.moveUp();
				reader.moveDown();
				childReq.setDescription(reader.getValue());
				logger.debug("Child Desc : " + reader.getValue());
				reader.moveUp();
				reader.moveDown();
				childReq.setStoredProcedure(reader.getValue());
				logger.debug("Child Proc : " + reader.getValue());
				reader.moveUp();
				reader.moveUp(); 
				newReq.setChild(childReq.clone());
				childReq.clear();
			}
			reader.moveUp();
			reqList.add(newReq.clone());
			newReq.clear();
			
		
			try{
				if (reader.hasMoreChildren()){
					reader.moveDown();
				}
			}catch (Exception e ){
				reader.close();
				isComplete = true;
			}
	
		}
		
		reqConfig.setRequestList(reqList);
		return reqList;
	}

}
