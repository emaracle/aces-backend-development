package ca.kflapublichealth.output;

public class UserData {
	public String name;
	public String useruuid;
	public String userRole;
	public String token;
	
	public UserData(String name,String useruuid,String userRole,String token) {
		this.name = name;
		this.useruuid = useruuid;
		this.userRole = userRole;
		this.token = token;
	}
}