package ca.kflapublichealth.output;

public class AppSettings {

	public Boolean forcePasswordChange;
	public String passwordLastUpdated;
	public LandingPageSettings landingPageSettings;
	
	public AppSettings(Boolean forcePasswordChange, String passwordLastUpdated, LandingPageSettings landingPageSettings) {
		this.forcePasswordChange = forcePasswordChange;
		this.passwordLastUpdated = passwordLastUpdated;
		this.landingPageSettings = landingPageSettings;
	}
}