package ca.kflapublichealth.helpers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import ca.kflapublichealth.connections.DatabaseConnection;
import ca.kflapublichealth.requests.Request;
import ca.kflapublichealth.requests.RequestReference;

public class AcesSetting {
	private static RequestReference reqReference = new RequestReference();

	/**
	 * Get multiple settings
	 * @param settingKeys
	 * @return
	 * @throws SQLException
	 */
	public static HashMap<String, String> getSettings(List<String> settingKeys) throws SQLException {
		//create map of all the setting keys
		//value will be used to check if it's value has been initialized
		HashMap<String, String> resultValues = new HashMap<>(); 

		//create comma delimited string setting keys to be used as param for sproc 
		String strSettingKeys = "";
		for(String key: settingKeys) {
			if (!strSettingKeys.isEmpty()) strSettingKeys += ",";
			strSettingKeys += key;
			
			resultValues.put(key, null);
		}
		
		Request request = reqReference.getRequest("getSettings");
		LinkedHashMap<String, Object> sqlParams = new LinkedHashMap<>();
		sqlParams.put("SettingKeys", strSettingKeys);
		
		ResultSet result = DatabaseConnection.getData(request.getStoredProcedure(), sqlParams);
		while (result.next()) {
			String key = result.getString(1);
			if (resultValues.containsKey(key)) {
				resultValues.put(key, result.getString(2));
			}
		}
		return resultValues;
	}
	
	/**
	 * Get value of single setting
	 * @param settingKey
	 * @return
	 * @throws SQLException
	 */
	public static String getSetting(String settingKey) throws SQLException {
		Request request = reqReference.getRequest("getSettings");
		LinkedHashMap<String, Object> sqlParams = new LinkedHashMap<>();
		sqlParams.put("SettingKey", settingKey);
		ResultSet result = DatabaseConnection.getData(request.getStoredProcedure(), sqlParams);
		if (result.next()) {
			return result.getString(1);
		} else {
			return null;
		}
	}
	
	/**
	 * Update a setting value
	 * @param settingKey
	 * @param settingValue
	 * @return
	 * @throws SQLException
	 */
	public static boolean updateSetting(String settingKey, String settingValue) throws SQLException {
		Request request = reqReference.getRequest("updateSetting");
		LinkedHashMap<String, Object> sqlParams = new LinkedHashMap<>();
		sqlParams.put("SettingKey", settingKey);
		sqlParams.put("SettingValue", settingValue);
		
		ResultSet result = DatabaseConnection.getData(request.getStoredProcedure(), sqlParams);
		if (result.next()) {
			if (result.getInt(1) == 1) {
				return true;
			}
		}
		return false;
	}
}
