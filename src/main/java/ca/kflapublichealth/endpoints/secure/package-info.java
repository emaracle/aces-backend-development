/** Handles all RESTful services requests that require authentication.
 */
package ca.kflapublichealth.endpoints.secure;